
# Consensys Demo booking app
![Mobile UI](/documentation/ui-desktop-chrome.gif)
![Desktop UI](/documentation/ui-mobile-chrome.gif)

# Quick Start

```bash
./letsgo.sh
```

![Dependency Graph](/documentation/graph.png)

![Architecture](/documentation/architecture.png)

## Build images
- main docker file needs to be rebuilt everytime a package is added / removed / upgraded.
```bash
yarn 
docker build . -t bookme:nx-base
docker-compose build
```

## Run with Docker
```bash
docker-compose --env-file '.env.compose' up 
```

## Run Locally
- you should have a local postgres running and configured settings in the ".env" file.
```bash
yarn
npx nx run-many --target=serve --projects=microservices-auth,microservices-users,microservices-rooms,microservices-reservations,gateway --parallel --maxParallel=10
npx nx run demo
```

### Backend API e2e testing
- The servers should be running before to run the e2e tests.
```bash
npx nx run-many --target=test --projects=microservices-auth,microservices-users,microservices-rooms,microservices-reservations,gateway --parallel --maxParallel=10
```
- Tests can also be run individually or in watch mode for TDD.
```bash
 npx nx test gateway --watch --test-file=auth-e2e.spec.ts
 npx nx test gateway --watch --test-file=reservations-e2e.spec.ts
 npx nx test gateway --watch --test-file=rooms-e2e.spec.ts
```
![Backend e2e](/documentation/testing-backend-e2e.png)

# WEB e2e testing
- Video recordings of each tests are available in `apps/demo-e2e/test-results/`
- Report is available in `apps/demo-e2e/playwright-report/`
```bash
npx nx test demo-e2e 
```
![web e2e](/documentation/web-e2e.png)
![web e2e report](/documentation/web-e2e-report.png)

## What to demonstrate?
- Monorepo
- Auth microservice with rotating tokens
- external lib integration
- Different type of microservices: REST, GRPC, WebSocket
- reservations testing cases
- Full dockerization
- Production logging capability with request / session tracing
  - can easily be integrated to EFK (Elasticsearch / Fluentd / Kibana)
- API Gateway can be replaced with an istio ingress and deployed to kubernetes cluster for scalability tests.
- Application cache by looking at the network tab and select / deselect rooms
- e2e and unit tests for backend
- web e2e with playwright, automatic video recording of test cases.


### Preventing overlaping reservations
Simplified the code to use sql OVERLAPS keyword instead of manually building a where statement.
- Here is an extract from reservatiosn test case:
``` ts
    /**
     *      [+++++++++++]           10-11am
     *    [------]                  A 9:50am-10:20am  BAD
     *              [----]          B 10:50am-11:10am BAD
     *        [----]                C 10:20am-10:40am BAD
     *   [-----------------]        D 9:50am-11:30am  BAD
     * [--]                         E 9:30am-9:55AM   GOOD
     *                    [----]    F 11:10am-11:30am GOOD
     *
     */
```
