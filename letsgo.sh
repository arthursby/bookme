#!/bin/bash

yarn 
docker build . -t bookme:nx-base
docker-compose build 

docker-compose up -d

echo "Waiting for services to start..."
sleep 3
echo "Demo should be available on http://localhost:4900" 
