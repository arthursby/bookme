import {
  CreateReservationDto,
  DeleteReservationDto,
  FindReservationDto,
  IPaginatedResult,
  IReservation,
  IReservationsService,
  MAX_QUERY_LIMIT,
  PaginationParams,
  UpdateReservationDto,
  VerifyReservationDto,
  VerifyReservationResultDto,
} from '@bookme/booklib';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import {
  DeleteResult,
  FindManyOptions,
  In,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';
import { Reservation } from './entities/reservation.entity';

@Injectable()
export class ReservationsService implements IReservationsService {
  private readonly logger = new Logger(ReservationsService.name);

  constructor(
    @InjectRepository(Reservation)
    private reservationsRepository: Repository<Reservation>
  ) {}

  async find(
    findReservationDto?: FindReservationDto,
    params?: PaginationParams
  ): Promise<IPaginatedResult<IReservation>> {
    const roomIds = findReservationDto?.roomIds || [];
    const skip = params?.offset || 0;
    const startId = params?.startId;
    const take = params?.limit || MAX_QUERY_LIMIT;
    const search = params?.search;

    const where: FindManyOptions<Reservation>['where'] = {};
    let separateCount = 0;

    if (search) {
      // where.email = Like(`%${search}%`);
    }
    // this.logger.debug(`type[0]==${typeof roomIds[0] }, roomIds`);
    if (roomIds.length > 0) {
      where.roomId = In(roomIds);
    }

    if (findReservationDto.start) {
      const start = new Date(findReservationDto.start);
      where.start = MoreThanOrEqual(start.toLocaleString());
    }

    if (findReservationDto.end) {
      const end = new Date(findReservationDto.end);
      where.end = LessThanOrEqual(end.toLocaleString());
    }

    if (startId) {
      where.id = MoreThan(startId);
      separateCount = await this.reservationsRepository.count();
    }

    const queryOptions: FindManyOptions<Reservation> = {
      where: where,
      skip: skip,
      take: take,
    };
    if (params?.order) {
      const orderFilter = {};
      params?.order.forEach((o, index) => {
        let direction = 'ASC'; // default if not found
        if (params.direction && params.direction.length >= index) {
          direction = params.direction[index];
        }
        orderFilter[params.order[index]] = direction;
      });

      queryOptions.order = orderFilter;
    }

    const [reservations, count] =
      await this.reservationsRepository.findAndCount(queryOptions);
    return {
      data: reservations,
      total: startId ? separateCount : count,
      current: skip / take + 1,
    };
  }

  async create(reservationDto: CreateReservationDto): Promise<IReservation> {
    // make sure it's not in the past...
    if (new Date() < reservationDto.start)
      throw new RpcException(`Invalid start date`);

    try {
      const reservation = await this.reservationsRepository.save(
        reservationDto
      );
      return reservation;
    } catch (error) {
      throw new RpcException("Can't create reservation");
    }
  }

  findById(id: number): Promise<IReservation> {
    return this.reservationsRepository.findOne(id);
  }

  async update(reservationDto: UpdateReservationDto): Promise<IReservation> {
    const reservation = await this.reservationsRepository.save({
      id: reservationDto.id,
      ...reservationDto,
    });
    return reservation;
  }

  async verifyAvailability(
    reservationInfo: VerifyReservationDto
  ): Promise<VerifyReservationResultDto> {
    const { start, roomId, end } = reservationInfo;

    const t1 = new Date(start).toLocaleString();
    const t2 = new Date(end).toLocaleString();
    this.logger.debug(`Check room=${roomId} start=${t1} end=${t2}`);

    const res = await this.reservationsRepository.query(
      `SELECT count(id) FROM "reservation" WHERE "roomId"=$1 and ("start","end") OVERLAPS ($2,$3)`,
      [roomId, t1, t2]
    );
    const overlappingReservations = await this.reservationsRepository.query(
      `SELECT *  FROM "reservation" WHERE "roomId"=$1 and ("start","end") OVERLAPS ($2,$3) LIMIT 5`,
      [roomId, t1, t2]
    );

    const count = parseInt(res[0].count);
    return { count: count, reservations: overlappingReservations };
  }

  async remove(
    deleteReservationDto: DeleteReservationDto
  ): Promise<DeleteResult> {
    const foundReservation = await this.reservationsRepository.findOne(
      deleteReservationDto.id
    );
    if (!foundReservation) {
      throw new HttpException('Not found', HttpStatus.BAD_REQUEST);
    } else if (foundReservation.userId != deleteReservationDto.userId) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    // const delResult = await this.reservationsRepository.createQueryBuilder().delete().where({ userId: deleteReservationDto.userId, id: deleteReservationDto.id }).execute();
    const delResult = await this.reservationsRepository.delete(
      deleteReservationDto.id
    );

    return delResult;
  }
}
