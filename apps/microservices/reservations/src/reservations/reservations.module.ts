import { LoggingInterceptor } from '@bookme/services/nest';
import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import { environment } from '../environments/environment';
import { Reservation } from './entities/reservation.entity';
import { ReservationsController } from './reservations.controller';
import { ReservationsService } from './reservations.service';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          // forRoutes: [{ path: '*', method: RequestMethod.ALL }],
          pinoHttp: {
            level: environment.production ? 'info' : 'debug',
            autoLogging: true,
            // install 'pino-pretty' package in order to use the following option
            transport: environment.production
              ? undefined
              : {
                  target: 'pino-pretty',
                  options: {
                    // genReqId: (req) => {
                    //   return req.id;
                    // },
                    colorize: true,
                    singleLine: true,
                    levelFirst: true,
                    ignore: 'hostname',
                    translateTime: 'UTC:yyyy/mm/dd HH:MM:ss TT Z',
                    sync: true,
                  },
                },
          },
        };
      },
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          database: configService.get('DB_DATABASE'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          autoLoadEntities: true,
          logging: !environment.production,
          synchronize: !environment.production,
        };
      },
    }),
    TypeOrmModule.forFeature([Reservation]),
  ],
  controllers: [ReservationsController],
  providers: [
    ReservationsService,
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class ReservationsModule {}
