import { IReservation } from '@bookme/booklib';
import { Check, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@Index(['userId', 'roomId'])
@Check('"start" < "end"')
export class Reservation implements IReservation {
  @PrimaryGeneratedColumn()
  id: number;
  @Index()
  @Column({ nullable: false })
  start: Date;
  @Column({ nullable: false })
  end: Date;
  // no foreign keys since we are in isolated microservices
  @Index()
  @Column({ nullable: false })
  userId: number; // user id
  @Index()
  @Column({ nullable: false })
  roomId: number;
  @Column()
  title: string;
  @Column()
  color?: string;
}
