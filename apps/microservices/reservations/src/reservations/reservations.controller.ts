import {
  CreateReservationDto,
  DeleteReservationDto,
  FindReservationDto,
  IRPCResponse,
  PaginationParams,
  UpdateReservationDto,
  VerifyReservationDto,
} from '@bookme/booklib';
import { TrackedPayload } from '@bookme/services/nest';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Logger,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ReservationsService } from './reservations.service';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class ReservationsController {
  private readonly logger = new Logger(ReservationsController.name);

  constructor(private readonly reservationsService: ReservationsService) {}

  @Post('create')
  async create(
    @Body() reservationDto: CreateReservationDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.create(reservationDto);
      response = {
        status: HttpStatus.CREATED,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: err,
        message: err.mmessage,
      };
    }
    return response;
  }

  @Post('find')
  async findAll(
    @TrackedPayload() params: PaginationParams,
    @Body() findReservatioDto?: FindReservationDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.find(
        findReservatioDto,
        params
      );
      response = {
        status: HttpStatus.OK,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        ...err,
      };
    }
    return response;
  }

  @Get('find-one')
  async findOne(@Query() id: number): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.findById(id);
      response = {
        status: HttpStatus.OK,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        ...err,
      };
    }
    return response;
  }

  @Post('update')
  async update(
    @Body() updateReservationDto: UpdateReservationDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.update(updateReservationDto);
      response = {
        status: HttpStatus.OK,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        ...err,
      };
    }
    return response;
  }

  @Post('verify')
  async verify(@Body() reservationInfo: VerifyReservationDto) {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.verifyAvailability(
        reservationInfo
      );
      response = {
        status: HttpStatus.OK,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        ...err,
      };
    }
    return response;
  }

  @Delete(':id')
  async remove(
    @Param('id') id: number,
    @Query() deleteReservationDto: DeleteReservationDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.reservationsService.remove(deleteReservationDto);
      response = {
        status: HttpStatus.OK,
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: err.status,
        error: err,
      };
    }
    return response;
  }
}
