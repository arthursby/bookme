import { MockType } from '@bookme/services/testing';
import { createMock } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { Reservation } from './entities/reservation.entity';
import { ReservationsController } from './reservations.controller';
import { ReservationsService } from './reservations.service';

describe('ReservationsController', () => {
  let controller: ReservationsController;

  beforeEach(async () => {
    const reservationsServiceMock = createMock<ReservationsService>();
    let reservationssRepository: MockType<Repository<Reservation>>;

    // const reservationsServiceMock = {
    //   findById: jest.fn().mockResolvedValue({}),
    //   findByEamil: jest.fn().mockResolvedValue({}),
    //   createUser: jest.fn().mockResolvedValue({}),
    //   remove: jest.fn(),
    // };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservationsController],
      providers: [ReservationsService],
    })
      .useMocker((mocker) => {
        console.log(`mocker ${typeof mocker} `, mocker);
        if (mocker == ReservationsService) {
          return reservationsServiceMock;
        } else if (typeof mocker === 'string') {
          // console.log(`trouble...`);
          return {};
        }
      })
      .compile();

    controller = module.get<ReservationsController>(ReservationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
