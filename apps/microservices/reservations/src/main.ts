import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Logger, PinoLogger } from 'nestjs-pino';
import { ReservationsModule } from './reservations/reservations.module';

async function bootstrap() {
  const port = new ConfigService().get('RESERVATIONS_SERVICE_PORT');

  const app = await NestFactory.create(ReservationsModule, {
    bufferLogs: false,
  });
  app.useLogger(app.get(Logger));

  await app.listen(port);
  const logger = new PinoLogger({});
  logger.info(`🚀 Reservations service running on port ${port}`);
}

bootstrap();
