export const environment = {
  production: true,
  typeORMConfig: {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'bookme',
    password: 'bookmesafely',
    database: 'bookme',
    synchronize: false,
    autoLoadEntities: true,
  }
};
