export const environment = {
  production: false,
  typeORMConfig: {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'bookme',
    password: 'bookmesafely',
    database: 'bookme',
    synchronize: true,
    autoLoadEntities: true,
  }
};
