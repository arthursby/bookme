import { CreateUserDto, IRPCResponse } from '@bookme/booklib';
import {
  ITrackedPayload,
  TrackedPayload,
  UserExistsException,
} from '@bookme/services/nest';
import {
  ClassSerializerInterceptor,
  Controller,
  HttpStatus,
  Logger,
  UseInterceptors,
} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { UsersService } from './users.service';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  private readonly logger = new Logger(UsersController.name);

  constructor(private readonly usersService: UsersService) {}

  @MessagePattern({ cmd: 'create-user' })
  async create(
    @TrackedPayload() createUserDto: CreateUserDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.usersService.createUser(createUserDto);
      response = {
        status: HttpStatus.CREATED,
        message: 'create-user_success',
        data: data,
        error: null,
      };
    } catch (err) {
      if (err instanceof UserExistsException) {
        response = {
          status: HttpStatus.CONFLICT,
          error: err.getError(),
          message: err.message,
        };
      } else {
        response = {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          ...err,
        };
      }
    }
    return response;
  }

  @MessagePattern({ cmd: 'find-by-id' })
  async findById(@TrackedPayload() id: number): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.usersService.findById(id);

      response = {
        status: HttpStatus.OK,
        message: 'findbyid_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'findbyid_error',
        data: null,
        error: err,
      };
    }
    return response;
  }

  @MessagePattern({ cmd: 'find-by-email' })
  async findByEmail(@TrackedPayload() email: string): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.usersService.findByEmail(email);
      response = {
        status: HttpStatus.OK,
        message: 'remove-user_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'remove-user-_error',
        data: null,
        error: err,
      };
    }
    return response;
  }

  @MessagePattern({ cmd: 'remove-user' })
  async remove(
    @Payload() payload: ITrackedPayload<number>
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const id = payload.data;
      const data = await this.usersService.remove(id);
      response = {
        status: HttpStatus.OK,
        message: 'remove-user_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'remove-user-_error',
        data: null,
        error: err,
      };
    }
    return response;
  }
}
