import { CreateUserDto, IUser, IUsersService, PostgresErrorCode } from '@bookme/booklib';
import { UserExistsException } from '@bookme/services/nest';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService implements IUsersService {
  private readonly logger = new Logger(UsersService.name);

  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {}

  async findById(id: number): Promise<IUser> {
    return this.usersRepository.findOne(id);
  }

  async findByEmail(email: string): Promise<IUser> {
    const users = await this.usersRepository.find({
      where: { email: email },
      take: 1,
    });
    if (users.length == 0) {
      throw new NotFoundException();
    }

    return users[0];
  }

  async createUser(createUserDto: CreateUserDto): Promise<IUser> {
    try {
      const user = await this.usersRepository.save(createUserDto);

      return user;
    } catch (error) {
      if (error?.code === PostgresErrorCode.UniqueViolation) {
        throw new UserExistsException();
      }
      throw new RpcException('unknown error');
    }
  }

  async remove(id: number) {
    return this.usersRepository.delete(id);
  }
}
