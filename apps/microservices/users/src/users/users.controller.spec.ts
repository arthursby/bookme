import { IUser } from '@bookme/booklib';
import { userCreateSuccess } from '@bookme/services/testing';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

const defaultUser: IUser = {
  email: 'test@test.com',
  password: 'somethingencrypted',
  id: 1,
};

const successResponse = {
  status: HttpStatus.CREATED,
  message: 'create-user_success',
  data: defaultUser,
  error: null,
};

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const usersServiceMock = {
      findById: jest.fn().mockResolvedValue(defaultUser),
      findByEamil: jest.fn().mockResolvedValue(defaultUser),
      createUser: jest.fn().mockResolvedValue(defaultUser),
      remove: jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      // providers: [UsersServiceMock],
    })
      .useMocker((token) => {
        if (token === UsersService) {
          return usersServiceMock;
        }
      })
      .compile();

    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call service methods', async () => {
    const createResultResponseSuccess = await controller.create(
      userCreateSuccess
    );
    expect(createResultResponseSuccess.status).toEqual(HttpStatus.CREATED);
  });
});
