import { IUser } from '@bookme/booklib';
import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;
  @Index()
  @Column({ unique: true })
  email: string;
  @Column()
  password: string;
}
