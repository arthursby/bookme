import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger, PinoLogger } from 'nestjs-pino';
import { UsersModule } from './users/users.module';

async function bootstrap() {
  const port = new ConfigService().get('USERS_SERVICE_PORT');

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    UsersModule,
    {
      transport: Transport.TCP,
      bufferLogs: true,
      options: {
        host: '0.0.0.0',
        port: port,
      },
    }
  );
  app.useLogger(app.get(Logger));

  await app.listen();
  const logger = new PinoLogger({});
  logger.info(`🚀 users microservice running on port ${port}`);
}

bootstrap();
