import { LoggingInterceptor } from '@bookme/services/nest';
import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { LoggerModule } from 'nestjs-pino';
import { environment } from '../environments/environment';
import { RoomsController } from './rooms.controller';
import { RoomsService } from './rooms.service';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    LoggerModule.forRoot({
      // forRoutes: [{ path: '*', method: RequestMethod.ALL }],
      pinoHttp: {
        level: environment.production ? 'info' : 'debug',
        autoLogging: false,
        // install 'pino-pretty' package in order to use the following option
        transport: environment.production
          ? undefined
          : {
              target: 'pino-pretty',
              options: {
                // genReqId: (req) => {
                //   return req.id;
                // },
                colorize: true,
                singleLine: true,
                levelFirst: true,
                ignore: 'hostname',
                translateTime: 'UTC:yyyy/mm/dd HH:MM:ss TT Z',
                sync: true,
              },
            },
      },
    }),
  ],
  controllers: [RoomsController],
  providers: [
    RoomsService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
  ],
})
export class RoomsModule {}
