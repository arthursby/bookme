import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';

import { RoomsService } from './rooms.service';

@Controller()
export class RoomsController {
  constructor(private readonly roomsService: RoomsService) {}

  @GrpcMethod('RoomsService', 'getAllRooms')
  async getAllRooms() {
    return this.roomsService.getAllRooms();
  }
}
