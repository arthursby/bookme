import { IRoomList, IRoomsService } from '@bookme/booklib';
import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class RoomsService implements IRoomsService {
  getAllRooms(): Observable<IRoomList> {
    return new Observable<IRoomList>((observer) => {
      observer.next({
        rooms: [
          { id: 1, name: 'Room 1', color: '#D6EAF8' },
          { id: 2, name: 'Room 2', color: '#5D6D7E' },
          { id: 3, name: 'Room 3', color: '#F5B7B1' },
          { id: 4, name: 'Room 4', color: '#76D7C4' },
          { id: 5, name: 'CEO Room', color: '#F7DC6F' },
          { id: 6, name: 'Special Room', color: '#34495E' },
          { id: 7, name: 'Mush room', color: '#EBDEF0' },
        ],
      });
      observer.complete();
    });
  }
}
