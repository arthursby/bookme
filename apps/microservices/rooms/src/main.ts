import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger, PinoLogger } from 'nestjs-pino';
import { join } from 'path';
import { RoomsModule } from './app/rooms.module';

async function bootstrap() {
  const configService = new ConfigService();
  const url = configService.get('ROOMS_GRPC_CONNECTION_URL');
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    RoomsModule,
    {
      transport: Transport.GRPC,
      bufferLogs: true,
      options: {
        package: configService.get('GRPC_PACKAGE'),
        protoPath: join(
          process.cwd(),
          'dist/apps/microservices/rooms/assets/rooms.api.proto'
        ),
        url: url,
      },
    }
  );
  app.useLogger(app.get(Logger));
  await app.listen();
  const logger = new PinoLogger({});
  logger.info(`🚀 Rooms microservice running on ${url}`);
}

bootstrap();
