import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger, PinoLogger } from 'nestjs-pino';
import { AuthModule } from './auth/auth.module';

async function bootstrap() {
  const port = new ConfigService().get('AUTH_SERVICE_PORT');

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AuthModule,
    {
      transport: Transport.TCP,
      bufferLogs: true,
      options: {
        host: '0.0.0.0',
        port: port,
      },
    }
  );
  app.useLogger(app.get(Logger));

  await app.listen();
  const logger = new PinoLogger({});
  logger.info(`🚀 Auth microservice running on port ${port}`);
}

bootstrap();
