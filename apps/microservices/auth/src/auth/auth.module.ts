import { LoggingInterceptor } from '@bookme/services/nest';
import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { LoggerModule } from 'nestjs-pino';
import { environment } from '../environments/environment';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TokenModule } from './token/token.module';

@Module({
  imports: [
    LoggerModule.forRoot({
      // forRoutes: [{ path: '*', method: RequestMethod.ALL }],
      pinoHttp: {
        level: environment.production ? 'info' : 'debug',
        autoLogging: false,
        // install 'pino-pretty' package in order to use the following option
        transport: environment.production
          ? undefined
          : {
              target: 'pino-pretty',
              options: {
                // genReqId: (req) => {
                //   return req.id;
                // },
                colorize: true,
                singleLine: true,
                levelFirst: true,
                ignore: 'hostname',
                translateTime: 'UTC:yyyy/mm/dd HH:MM:ss TT Z',
                sync: true,
              },
            },
      },
    }),
    PassportModule,
    ConfigModule.forRoot({ isGlobal: true }),
    TokenModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    {
      provide: 'USERS_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
            host: configService.get('USERS_SERVICE_HOST'),
            port: configService.get('USERS_SERVICE_PORT'),
          },
        });
      },
      inject: [ConfigService],
    },
    // {provide: APP_FILTER, useClass: AllExceptionsFilter},
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
