import {
  ITokenPayload,
  ITokenResponse,
  ITokenService,
  IUser,
} from '@bookme/booklib';
import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { environment } from '../../environments/environment';
import { Token } from './entities/token.entity';

@Injectable()
export class TokenService implements ITokenService {
  private readonly logger = new Logger(TokenService.name);

  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(Token) private readonly tokenRepository: Repository<Token>
  ) {}

  async createToken(user: IUser): Promise<ITokenResponse> {
    const accessTokenExpiresIn = environment.jwt.accessTokenExpiresIn;
    const refreshTokenExpiresIn = environment.jwt.refreshTokenExpiresIn;

    const now = new Date().getTime();
    const payload: ITokenPayload = {
      sub: '' + user.id,
      user_id: user.id,
      user: user,
      creation: now,
    };
    const refreshPayload: ITokenPayload = { ...payload, creation: now };
    const accessToken = this.jwtService.sign(payload, {
      secret: environment.jwt.secret,
      expiresIn: accessTokenExpiresIn,
    });
    const refreshToken = this.jwtService.sign(refreshPayload, {
      secret: environment.jwt.secret,
      expiresIn: refreshTokenExpiresIn,
    });

    await this.tokenRepository.save({
      user_id: user.id,
      access_token: accessToken,
      expires_at: new Date(now + accessTokenExpiresIn * 1000),
      refresh_token: refreshToken,
      refresh_expires_at: new Date(now + refreshTokenExpiresIn * 1000),
    });

    return {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: accessTokenExpiresIn,
      refresh_token_expires_in: refreshTokenExpiresIn,
      user: user,
    };
  }

  async verifyToken(token: string): Promise<ITokenPayload> {
    return await this.jwtService.verify(token);
  }

  deleteTokenForUserId(userId: number) {
    return this.tokenRepository.delete(userId);
  }

  decodeToken(token: string) {
    return this.jwtService.decode(token);
  }
}
