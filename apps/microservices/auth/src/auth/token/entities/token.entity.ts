import { IToken } from '@bookme/booklib';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Token implements IToken {
  @PrimaryColumn()
  user_id: number;
  @Column()
  access_token: string;
  @Column()
  expires_at: Date;
  @Column()
  refresh_token: string;
  @Column()
  refresh_expires_at: Date;
}
