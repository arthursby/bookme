import {
  ILogin,
  IRegister,
  IRPCResponse, SilentLoginDto
} from '@bookme/booklib';
import { TrackedPayload, UserExistsException } from '@bookme/services/nest';
import {
  ClassSerializerInterceptor, Controller, HttpStatus, Logger, UseInterceptors
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AuthService } from './auth.service';
import { TokenService } from './token/token.service';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
  private readonly logger = new Logger(AuthController.name);
  constructor(
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
  ) {}

  @MessagePattern({ cmd: 'register' })
  async register(
    @TrackedPayload() registerUserDto: IRegister
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;

    try {
      const data = await this.authService.register(registerUserDto);
      response = {
        status: HttpStatus.CREATED,
        message: 'register_success',
        data: data,
        error: null,
      };
    } catch (err) {
      if (err instanceof UserExistsException) {
        return {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: err.message,
          message: '',
          data: null,
        };
      }
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'register_error',
        data: null,
        error: err,
      };
    }

    return response;
  }

  @MessagePattern({ cmd: 'login' })
  async login(
    @TrackedPayload() credentials: ILogin
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;
    try {
      const data = await this.authService.login(credentials);
      response = {
        status: HttpStatus.OK,
        message: 'login_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.FORBIDDEN,
        message: 'login_error',
        data: null,
        error: err,
      };
    }

    return response;
  }

  @MessagePattern({ cmd: 'silent-login' })
  async silentLogin(
    @TrackedPayload() credentials: SilentLoginDto
  ): Promise<IRPCResponse> {
    let response: IRPCResponse;
    try {
      const data = await this.authService.silentLogin(credentials);
      response = {
        status: HttpStatus.OK,
        message: 'silent-login_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.BAD_REQUEST,
        message: 'silent-login_error',
        data: null,
        error: err,
      };
    }

    return response;
  }

  @MessagePattern({ cmd: 'verify' })
  async verify(@TrackedPayload() token: string): Promise<IRPCResponse> {
    let response: IRPCResponse;
    try {
      const data = await this.tokenService.verifyToken(token);
      response = {
        status: HttpStatus.OK,
        message: 'verify_success',
        data: data,
        error: null,
      };
    } catch (err) {
      response = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'verify_error',
        data: null,
        error: err,
      };
    }

    return response;
  }

  @MessagePattern({ cmd: 'logout' })
  async logout(@TrackedPayload() userId: number): Promise<IRPCResponse> {
    this.authService.logout(userId);
    const response: IRPCResponse = {
      status: HttpStatus.OK,
      message: 'logout_success',
      data: null,
      error: null,
    };
    return response;
  }

  @MessagePattern({ cmd: 'test' })
  async test(@TrackedPayload() payload:string): Promise<IRPCResponse> {
    const errorResponse:IRPCResponse = {
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      message: 'test_error',
      data: null,
      error: new Error('test'),
    };
    const successResponse:IRPCResponse = {
      status: HttpStatus.OK,
      message: 'test_success',
      data: "yeahhhh!!! "+payload,
      error: null,
    };

    return Promise.resolve(successResponse);
  }
}
