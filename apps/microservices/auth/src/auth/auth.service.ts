import {
  IAuthService,
  ILogin,
  IRegister,
  ITokenResponse,
  IUser,
  SilentLoginDto,
} from '@bookme/booklib';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import * as bcrypt from 'bcrypt';
import { firstValueFrom } from 'rxjs';
import { TokenService } from './token/token.service';

@Injectable()
export class AuthService implements IAuthService {
  private rpcTimeout: number;
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private configService: ConfigService,
    @Inject('USERS_SERVICE') private usersService: ClientProxy,
    private tokenService: TokenService
  ) {
    this.rpcTimeout = this.configService.get('RPC_TIMEOUT');
  }

  public logout(userId: number): void {
    this.tokenService.deleteTokenForUserId(userId);
  }

  public async register(registrationData: IRegister): Promise<ITokenResponse> {
    const hashedPassword = await bcrypt.hash(registrationData.password, 10);

    const createdUserResponse = await firstValueFrom(
      this.usersService.send(
        { cmd: 'create-user' },
        {
          ...registrationData,
          password: hashedPassword,
        }
      )
    );
    const createdUser: IUser = createdUserResponse.data;
    if (createdUserResponse.error) {
      throw createdUserResponse;
    }

    createdUser.password = undefined;

    const tokenResponse: ITokenResponse = await this.tokenService.createToken(
      createdUser
    );
    return tokenResponse;
  }

  public async silentLogin(
    credentials: SilentLoginDto
  ): Promise<ITokenResponse> {
    try {
      const { email, refreshToken } = credentials;
      const payload = await this.tokenService.verifyToken(refreshToken);
      if (email !== payload.user.email) {
        this.logger.log(
          `Invalid credentials for ${email} with token user [ id=${payload.sub}, email=${payload.user.email} ]  `
        );
        throw new RpcException(`Invalid credentials`);
      }
      const user = payload.user;

      // Rotate tokens to return a new bearer and update refresh token.
      const tokenResponse: ITokenResponse = await this.tokenService.createToken(
        user
      );
      return tokenResponse;
    } catch (error) {
      throw new RpcException('Invalid Refresh token');
    }
  }

  public async login(credentials: ILogin): Promise<ITokenResponse> {
    const { email, password } = credentials;
    try {
      const response = await firstValueFrom(
        this.usersService.send({ cmd: 'find-by-email' }, email)
      );
      const user: IUser = response.data;
      await this.verifyPassword(password, user.password);
      user.password = undefined;

      const tokenResponse = await this.tokenService.createToken(user);
      return tokenResponse;
    } catch (error) {
      throw new RpcException('Wrong credentials provided');
    }
  }

  private async verifyPassword(
    plainTextPassword: string,
    hashedPassword: string
  ) {
    const isPasswordMatching = await bcrypt.compare(
      plainTextPassword,
      hashedPassword
    );
    if (!isPasswordMatching) {
      throw new RpcException('Wrong credentials provided');
    }
  }
}
