export const environment = {
  production: false,
  jwt: {
    secret: "aadfkadjsfADSFASDFAdsfkjasdflQ32roir#@@#Ewqlrjwd@#EWQDlj21;2o3e0",
    accessTokenExpiresIn: 300, // 5min
    refreshTokenExpiresIn: 900 // 15min (should be several days in production)
  }
};
