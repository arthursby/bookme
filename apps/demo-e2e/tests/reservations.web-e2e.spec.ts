import { ParseEnumPipe } from '@nestjs/common';
import { test, expect, Page } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:4900');
});

test.describe('Auth', () => {
  test.beforeEach(async ({ page }) => {
    await (await page.waitForSelector('#reset-btn')).click();
    await page.waitForSelector('#login-btn');
    // await page.waitForTimeout(200);
  });

  test('should fail to login before setup', async ({ page }) => {
    // Wait for the page to load all rooms
    await page.waitForSelector('button.room-selector.selected');
    await page.click('#login-btn');
    // await page.waitForTimeout(100);
    await page.click('#login-submit');
    // await page.waitForTimeout(100);
    await page.waitForSelector('#alert-error-cred');
    const content = await page.content();
    const t = await page.textContent('#alert-error-cred');
    expect(t).toContain('Invalid Credentials');
  });

  test('should login successfully after setup', async ({ page }) => {
    await page.click('#setup-btn');
    // Wait for the events to load after setup
    await page.waitForSelector('div.rs__multi_day button');
    await page.click('#login-btn');
    // await page.waitForTimeout(100);
    await page.click('#login-submit');
    // await page.waitForTimeout(100);
    await page.waitForSelector('#logout-btn');
    const headerText = await page.textContent('#header');
    // console.debug('headerText', headerText);
    expect(headerText).toContain('demo@consensys.com');
  });
});

test.describe('Reservations', () => {
  test.beforeEach(async ({ page }) => {
    await (await page.waitForSelector('#reset-btn')).click();
    await page.waitForSelector('#login-btn');
  });

  test('setup should create default reservations', async ({
    page,
    browser,
  }) => {
    await page.click('#setup-btn');
    // Wait for the events to load after setup
    await page.waitForSelector('div.rs__multi_day button');
    await page.waitForTimeout(100);
    const found = (await page.$$('div.rs__multi_day button')).length;
    // console.debug(`found tests are`, found);
    expect(found).toBeGreaterThan(0);
  });

  test('should fail to add a reservation without login', async ({ page }) => {
    await page.click('#setup-btn');
    await page.waitForSelector('div.rs__multi_day button');
    await page.click('span.rs__cell > button');
    await page.waitForSelector('#editor');
    const editorText = await page.textContent('#editor');
    expect(editorText).toContain('You must be logged in to create an avent.');
  });

  test('should create a reservation after logging in', async ({ page }) => {
    await page.click('#setup-btn');
    // await page.waitForTimeout(200);
    await page.waitForSelector('div.rs__multi_day button');
    // await page.waitForTimeout(200);
    await page.click('#login-btn');
    // await page.waitForTimeout(200);
    await page.click('#login-submit');
    // await page.waitForTimeout(100);
    await page.waitForSelector('#logout-btn');
    // Wait for the grid button to be available
    await page.waitForSelector('div.rs__multi_day button');

    // await page.waitForTimeout(100);
    await page.click('span.rs__cell > button');
    // Click on a room to make sure at least one is selected
    // await page.click('button.room-selector');

    await page.waitForSelector('#editor');

    const totalBefore = parseInt(await (await page.$('#total')).inputValue());
    await page.fill('#editor-title', 'Created during e2e');
    await page.click('#editor-room');
    // await page.waitForTimeout(200);
    await page.click('ul.MuiList-root > li.MuiMenuItem-root');
    // "#room-select-label > li:first-child";
    // Wait for room is available confirmation message.
    await page.waitForSelector('#room-available');

    await page.waitForSelector('#notistack-snackbar', { state: 'detached' });
    await page.waitForSelector('#editor-confirm-btn:enabled');
    // force to true because on mobile the notification can get over the button and prevent clicking during testing otherwise.
    await page.click('#editor-confirm-btn:enabled');
    // Wait for the editor to close
    await page.waitForSelector('#editor', { state: 'detached' });
    await page.waitForTimeout(1000);
    // Add an extra 500ms to completely update the UI
    const totalAfter = parseInt(await (await page.$('#total')).inputValue());
    expect(totalAfter).toBe(totalBefore + 1);
  });
});
