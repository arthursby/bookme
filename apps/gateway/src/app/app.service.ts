import {
  CreateReservationDto,
  demoUser,
  demoUser2, IRoomsService,
  IUser
} from '@bookme/booklib';
import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientGrpc, ClientProxy } from '@nestjs/microservices';
import * as bcrypt from 'bcrypt';
import { add, addDays, addMinutes } from 'date-fns';
import { firstValueFrom } from 'rxjs';
import { Connection } from 'typeorm';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  reservationServiceUrl: string;
  private roomsService: IRoomsService;

  constructor(
    private connection: Connection,
    private httpService: HttpService,
    private configService: ConfigService,
    @Inject('AUTH_SERVICE') private authServiceClient: ClientProxy,
    @Inject('ROOMS_SERVICE') private client: ClientGrpc,
    @Inject('USERS_SERVICE') private usersService: ClientProxy
  ) {
    const host = configService.get('RESERVATIONS_SERVICE_HOST');
    const port = configService.get('RESERVATIONS_SERVICE_PORT');
    this.reservationServiceUrl = `http://${host}:${port}`;
  }

  onModuleInit() {
    this.roomsService = this.client.getService<IRoomsService>('RoomsService');
  }

  async setup() {
    this.reset();
    // Create default user
    const hashedPassword = await bcrypt.hash(demoUser.password, 10);

    const createdUserResponse = await firstValueFrom(
      this.usersService.send(
        { cmd: 'create-user' },
        {
          ...demoUser,
          password: hashedPassword,
        }
      )
    );
    if (createdUserResponse.error) {
      throw new HttpException(`server error`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    const createdUser2Response = await firstValueFrom(
      this.usersService.send(
        { cmd: 'create-user' },
        {
          ...demoUser2,
          password: hashedPassword,
        }
      )
    );
    if (createdUser2Response.error) {
      throw new HttpException(`server error`, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    const createdDemoUser: IUser = createdUserResponse.data;
    const createdDemoUser2: IUser = createdUser2Response.data;
    const { rooms } = await firstValueFrom(this.roomsService.getAllRooms({}));
    const room1 = rooms.find((r) => r.id == 1);
    const room2 = rooms.find((r) => r.id == 1);
    const mushRoom = rooms.find((r) => r.name == 'Mush room');
    // Create reservations for testing
    const url = this.reservationServiceUrl + '/create';
    const today = addDays(new Date(), 1); // start from tomorrow

    const res1: CreateReservationDto = {
      start: addMinutes(today, 1),
      end: addMinutes(today, 90),
      userId: createdDemoUser.id,
      roomId: room1.id,
      title: 'demo res 1',
      color: room1.color,
    };
    const res2: CreateReservationDto = {
      start: addDays(today, 1),
      end: addDays(today, 2),
      userId: createdDemoUser2.id,
      roomId: room2.id,
      title: 'demo res 2',
      color: room2.color,
    };
    const res3: CreateReservationDto = {
      start: add(today, { hours: 1 }),
      end: add(today, { hours: 2 }),
      userId: createdDemoUser.id,
      roomId: mushRoom.id,
      title: 'demo res 3',
      color: mushRoom.color,
    };

    const res4: CreateReservationDto = {
      start: add(today, { days: 3 }),
      end: add(today, { days: 10 }),
      userId: createdDemoUser.id,
      roomId: mushRoom.id,
      title: 'demo res 4',
      color: mushRoom.color,
    };

    const res5: CreateReservationDto = {
      start: add(today, { days: 5 }),
      end: add(today, { days: 7 }),
      userId: createdDemoUser.id,
      roomId: room1.id,
      title: 'demo res 5',
      color: room1.color,
    };
    await firstValueFrom(this.httpService.post(url, res1));
    await firstValueFrom(this.httpService.post(url, res2));
    await firstValueFrom(this.httpService.post(url, res3));
    await firstValueFrom(this.httpService.post(url, res4));
    await firstValueFrom(this.httpService.post(url, res5));
  }

  async reset() {
    await this.connection.transaction(async (manager) => {
      await manager.query(`truncate "user" cascade;`);
      await manager.query(`truncate "token" cascade;`);
      await manager.query(`truncate "reservation" cascade;`);
    });
  }
}
