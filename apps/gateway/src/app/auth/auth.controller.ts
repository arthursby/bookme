import {
  IRPCResponse,
  ITracker,
  LogInDto,
  RegisterDto,
  SilentLoginDto,
} from '@bookme/booklib';
import { Tracker } from '@bookme/services/nest';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpException,
  Inject,
  Logger,
  Post,
  Query,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Response } from 'express';
import { firstValueFrom } from 'rxjs';
import { Public } from './decorators/public.decorator';

@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
  private readonly logger = new Logger(AuthController.name);

  constructor(@Inject('AUTH_SERVICE') private authServiceClient: ClientProxy) {}

  @Post('login')
  @Public()
  async login(
    @Res() response: Response,
    @Body() credentials: LogInDto,
    @Tracker() trackingInfo: ITracker
  ) {
    const serviceResponse: IRPCResponse = await firstValueFrom(
      this.authServiceClient.send(
        {
          cmd: 'login',
        },
        { data: credentials, tracking: trackingInfo }
      )
    );
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Get('logout')
  async logout(@Res() response: Response, @Tracker() trackingInfo: ITracker) {
    const serviceResponse: IRPCResponse = await firstValueFrom(
      this.authServiceClient.send(
        {
          cmd: 'logout',
        },
        { data: 1, tracking: trackingInfo }
      )
    );

    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Get('silent-login')
  @Public()
  async silentLogin(
    @Res() response: Response,
    @Query() credentials: SilentLoginDto,
    @Tracker() trackingInfo: ITracker
  ) {
    this.logger.debug(`inside silent login`);
    const serviceResponse: IRPCResponse = await firstValueFrom(
      this.authServiceClient.send(
        {
          cmd: 'silent-login',
        },
        { data: credentials, tracking: trackingInfo }
      )
    );
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Public()
  @Get('test')
  async test(@Res() response: Response, @Tracker() trackingInfo: ITracker) {
    console.log(`result is `, trackingInfo);
    const serviceResponse: IRPCResponse = await firstValueFrom(
      this.authServiceClient.send(
        {
          cmd: 'test',
        },
        { data: 'test', tracking: trackingInfo }
      )
    );
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Post('register')
  @Public()
  async register(
    @Res() response: Response,
    @Body() registration: RegisterDto,
    @Tracker() trackingInfo: ITracker
  ) {
    const serviceResponse: IRPCResponse = await firstValueFrom(
      this.authServiceClient.send(
        {
          cmd: 'register',
        },
        { data: registration, tracking: trackingInfo }
      )
    );
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }
}
