import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  UseInterceptors,
} from '@nestjs/common';
import { environment } from '../environments/environment';
import { AppService } from './app.service';
import { Public } from './auth/decorators/public.decorator';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly appService: AppService) {}

  @Public()
  @Get('reset')
  async reset() {
    if (environment.production) {
      throw new HttpException('not in prod!', HttpStatus.FORBIDDEN);
    }

    try {
      await this.appService.reset();
    } catch (err) {
      return 'an error occured';
    }

    return { data: 'boooom!' };
  }

  @Public()
  @Get('setup')
  async setup() {
    if (environment.production) {
      throw new HttpException('not in prod!', HttpStatus.FORBIDDEN);
    }

    try {
      await this.appService.setup();
    } catch (err) {
      return 'an error occured';
    }

    return { data: 'boooom!' };
  }
}
