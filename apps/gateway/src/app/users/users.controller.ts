import {
  ClassSerializerInterceptor,
  Controller,
  Inject,
  Logger,
  UseInterceptors,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  private readonly logger = new Logger(UsersController.name);

  constructor(@Inject('USERS_SERVICE') private usersService: ClientProxy) {}
}
