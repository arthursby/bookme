import { AllExceptionsFilter, LoggingInterceptor } from '@bookme/services/nest';
import { HttpModule } from '@nestjs/axios';
import { Module, RequestMethod, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Joi from 'joi';
import { LoggerModule } from 'nestjs-pino';
import { SessionModule } from 'nestjs-session';
import { join } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { environment } from '../environments/environment';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from './auth/auth.controller';
import { AuthGuard } from './auth/guards/auth.guard';
import { ReservationsController } from './reservations/reservations.controller';
import { RoomsController } from './rooms/rooms.controller';
import { UsersController } from './users/users.controller';

@Module({
  imports: [
    LoggerModule.forRoot({
      forRoutes: [{ path: '*', method: RequestMethod.ALL }],
      pinoHttp: {
        level: environment.production ? 'info' : 'debug',
        autoLogging: true,
        // install 'pino-pretty' package in order to use the following option
        transport: environment.production
          ? undefined
          : {
              target: 'pino-pretty',
              options: {
                // genReqId: (req) => {
                //   return uuidv4.uuid();
                // },
                colorize: true,
                levelFirst: true,
                singleLine: true,
                ignore: 'hostname',
                translateTime: 'UTC:yyyy/mm/dd HH:MM:ss TT Z',
                sync: true,
              },
            },
      },
    }),
    SessionModule.forRoot({
      session: {
        secret: 'session secret...',
        resave: false,
        genid: (req) => {
          return uuidv4();
        },
        saveUninitialized: false,
      },
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        AUTH_SERVICE_HOST: Joi.string().required(),
        AUTH_SERVICE_PORT: Joi.number().required(),
        USERS_SERVICE_HOST: Joi.string().required(),
        USERS_SERVICE_PORT: Joi.number().required(),
        RESERVATIONS_SERVICE_HOST: Joi.string().required(),
        RESERVATIONS_SERVICE_PORT: Joi.number().required(),
        GRPC_PACKAGE: Joi.string().required(),
        ROOMS_GRPC_CONNECTION_URL: Joi.string().required(),
        ROOMS_GRPC_PROTOPATH: Joi.string().required(),
        RPC_TIMEOUT: Joi.number().required(),
      }),
    }),
    HttpModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          database: configService.get('DB_DATABASE'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          autoLoadEntities: true,
          synchronize: !environment.production,
        };
      },
    }),
  ],
  controllers: [
    AuthController,
    RoomsController,
    UsersController,
    ReservationsController,
    AppController,
  ],
  providers: [
    AppService,
    {
      provide: 'AUTH_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
            host: configService.get('AUTH_SERVICE_HOST'),
            port: configService.get('AUTH_SERVICE_PORT'),
          },
        });
      },
      inject: [ConfigService],
    },
    {
      provide: 'TOKEN_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
            host: configService.get('TOKEN_SERVICE_HOST'),
            port: configService.get('TOKEN_SERVICE_PORT'),
          },
        });
      },
      inject: [ConfigService],
    },
    {
      provide: 'ROOMS_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            package: configService.get('GRPC_PACKAGE'),
            protoPath: join(
              process.cwd(),
              configService.get('ROOMS_GRPC_PROTOPATH')
            ),
            url: configService.get('ROOMS_GRPC_CONNECTION_URL'),
          },
        });
      },
      inject: [ConfigService],
    },
    {
      provide: 'USERS_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
            host: configService.get('USERS_SERVICE_HOST'),
            port: configService.get('USERS_SERVICE_PORT'),
          },
        });
      },
      inject: [ConfigService],
    },
    { provide: APP_GUARD, useClass: AuthGuard },
    // {provide: APP_FILTER, useClass: HttpExceptionsFilter},
    { provide: APP_FILTER, useClass: AllExceptionsFilter },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
  ],
})
export class AppModule {}
