import { IUser } from '@bookme/booklib';
import { Request } from 'express';

export interface RequestWithUser extends Request {
  user: IUser;
}
