import { IRoomsService } from '@bookme/booklib';
import {
  Controller,
  Get,
  HttpStatus,
  Inject,
  OnModuleInit,
  Res,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Response } from 'express';
import { firstValueFrom } from 'rxjs';
import { Public } from '../auth/decorators/public.decorator';

@Controller('rooms')
export class RoomsController implements OnModuleInit {
  private roomsService: IRoomsService;

  constructor(@Inject('ROOMS_SERVICE') private client: ClientGrpc) {}

  onModuleInit() {
    this.roomsService = this.client.getService<IRoomsService>('RoomsService');
  }

  @Get()
  @Public()
  async getAllRooms(@Res() response: Response) {
    const roomsList = await firstValueFrom(this.roomsService.getAllRooms({}));
    response.status(HttpStatus.OK).send(roomsList);
  }
}
