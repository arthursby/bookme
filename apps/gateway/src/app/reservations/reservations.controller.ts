import {
  CreateReservationDto,
  FindReservationDto,
  IRPCResponse,
  ITracker,
  VerifyReservationDto,
} from '@bookme/booklib';
import { Tracker } from '@bookme/services/nest';
import { HttpService } from '@nestjs/axios';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpException,
  Logger,
  Param,
  Post,
  Query,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
import { firstValueFrom } from 'rxjs';
import { Public } from '../auth/decorators/public.decorator';

@Controller('reservations')
@UseInterceptors(ClassSerializerInterceptor)
export class ReservationsController {
  private readonly logger = new Logger(ReservationsController.name);
  private baseUrl: string;
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {
    const host = configService.get('RESERVATIONS_SERVICE_HOST');
    const port = configService.get('RESERVATIONS_SERVICE_PORT');
    this.baseUrl = `http://${host}:${port}`;
  }

  @Public()
  @Get()
  async findAll(
    @Res() response: Response,
    @Tracker() trackingInfo: ITracker,
    @Query() findReservatioDto: FindReservationDto
  ) {
    const url = this.baseUrl + '/find';
    const headers = {
      'x-request-id': trackingInfo.requestId,
      'x-session-id': trackingInfo.sessionId,
    };
    if (trackingInfo.email) {
      headers['x-user-email'] = trackingInfo.email;
      headers['x-user-id'] = trackingInfo.userId;
    }
    const httpResponse = await firstValueFrom(
      this.httpService.post(url, findReservatioDto, {
        headers: headers,
      })
    );
    const serviceResponse: IRPCResponse = httpResponse.data;
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Post()
  async create(
    @Res() response: Response,
    @Body() registration: CreateReservationDto,
    @Tracker() trackingInfo: ITracker
  ) {
    const url = this.baseUrl + '/create';
    const headers = {
      'x-request-id': trackingInfo.requestId,
      'x-session-id': trackingInfo.sessionId,
    };
    if (trackingInfo.email) {
      headers['x-user-email'] = trackingInfo.email;
      headers['x-user-id'] = trackingInfo.userId;
    }
    const httpResponse = await firstValueFrom(
      this.httpService.post(url, registration, {
        headers: headers,
      })
    );
    const serviceResponse: IRPCResponse = httpResponse.data;
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Post('verify')
  async verify(
    @Res() response: Response,
    @Body() reservationInfo: VerifyReservationDto,
    @Tracker() trackingInfo: ITracker
  ) {
    const url = this.baseUrl + '/verify';
    const headers = {
      'x-request-id': trackingInfo.requestId,
      'x-session-id': trackingInfo.sessionId,
    };
    if (trackingInfo.email) {
      headers['x-user-email'] = trackingInfo.email;
      headers['x-user-id'] = trackingInfo.userId;
    }

    const options = {
      headers,
    };

    const httpResponse = await firstValueFrom(
      this.httpService.post(url, reservationInfo, options)
    );
    const serviceResponse: IRPCResponse = httpResponse.data;
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }

  @Delete(':id')
  async remove(
    @Res() response: Response,
    @Param('id') id: string,
    @Tracker() trackingInfo: ITracker
  ) {
    const headers = {
      'x-request-id': trackingInfo.requestId,
      'x-session-id': trackingInfo.sessionId,
    };
    if (trackingInfo.email) {
      headers['x-user-email'] = trackingInfo.email;
      headers['x-user-id'] = trackingInfo.userId;
    }

    const url = this.baseUrl + `/${id}?id=${id}&userId=${trackingInfo.userId}`;
    const options = {
      headers,
    };

    const httpResponse = await firstValueFrom(
      this.httpService.delete(url, options)
    );
    const serviceResponse: IRPCResponse = httpResponse.data;
    if (serviceResponse.error) {
      throw new HttpException(serviceResponse.error, serviceResponse.status);
    }
    response.status(serviceResponse.status).send(serviceResponse.data);
  }
}
