import { RequestMethod } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  Logger,
  PinoLogger,
  InjectPinoLogger,
  LoggerModule,
} from 'nestjs-pino';
import * as session from 'express-session';

import { AppModule } from './app/app.module';
import { ConfigService } from '@nestjs/config';
declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: false
  });
  app.enableCors();
  app.useLogger(app.get(Logger));
  const configService =app.get(ConfigService);

  const port = configService.get("GATEWAY_SERVICE_PORT") || process.env.PORT || 3334;
  await app.listen(port);
  const logger = new PinoLogger({});
  logger.info(`🚀 Application is running on: http://localhost:${port}`);


  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  
}

bootstrap();
