import { ITokenResponse } from '@bookme/booklib';
import {
  userCreateErrorInvalidEmail,
  userCreateErrorPasswordLength,
  userCreateSuccess,
} from '@bookme/services/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Client } from 'pg';
import * as request from 'supertest';
import { dbCredentials } from './common';

describe('Auth (e2e)', () => {
  let app: INestApplication;
  let dbClient: Client;
  let tokenResponse: ITokenResponse;
  const config = new ConfigService();
  const port = config.get('GATEWAY_SERVICE_PORT') || process.env.PORT || 3334;
  const baseUrl = `http://localhost:${port}`;

  beforeAll(async () => {
    console.log(`credentials are`, dbCredentials);
    dbClient = new Client(dbCredentials);
    await dbClient.connect();
    await resetDB();
  });

  const resetDB = async () => {
    await dbClient.query('truncate "user" cascade');
  };

  beforeEach(async () => {
    return true;
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
    return true;
  });

  afterAll(async () => {
    dbClient.end();
  });

  it('/auth/test (GET)', async () => {
    const response = await request(baseUrl).get('/auth/test');
    expect(response.status).toBe(HttpStatus.OK);

    return response;
  });

  it('/auth/register (POST) - should not pass validator without at body', async () => {
    const response = await request(baseUrl).post('/auth/register').send();
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/register (POST) - should fail if password is too short', async () => {
    const response = await request(baseUrl)
      .post('/auth/register')
      .send(userCreateErrorPasswordLength);
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/register (POST) - should fail if email is invalid', async () => {
    const response = await request(baseUrl)
      .post('/auth/register')
      .send(userCreateErrorInvalidEmail);
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/register (POST) - should create a valid user', async () => {
    const response = await request(baseUrl)
      .post('/auth/register')
      .send(userCreateSuccess);
    expect(response.status).toBe(HttpStatus.CREATED);
    expect(response.body.user.email).toBe(userCreateSuccess.email);
    return response;
  });

  it('/auth/login (POST) - should login with the new user', async () => {
    const response = await request(baseUrl)
      .post('/auth/login')
      .send(userCreateSuccess);
    expect(response.status).toBe(HttpStatus.OK);
    tokenResponse = response.body;
    return response;
  });

  it('/auth/login (POST) - should error with wrong credentials', async () => {
    const response = await request(baseUrl)
      .post('/auth/login')
      .send({ ...userCreateSuccess, password: 'different' });
    expect(response.status).toBe(HttpStatus.FORBIDDEN);
    return response;
  });

  it('/auth/silent-login (GET) - should rotate users token', async () => {
    const url = `/auth/silent-login?email=${userCreateSuccess.email}&refreshToken=${tokenResponse.refresh_token}`;
    const response = await request(baseUrl).get(url).send();
    expect(response.status).toBe(HttpStatus.OK);
    const newTokenResponse: ITokenResponse = response.body;
    expect(newTokenResponse.refresh_token).not.toBe(
      tokenResponse.refresh_token
    );
    expect(newTokenResponse.access_token).not.toBe(tokenResponse.access_token);
    tokenResponse = newTokenResponse;
    return response;
  });

  it('/auth/silent-login (GET) - should detect invalid token with length below 20 characters', async () => {
    const url = `/auth/silent-login?email=test@email.com&refreshToken=INVALID`;
    const response = await request(baseUrl).get(url).send();
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/silent-login (GET) - should error when token is invalid', async () => {
    const url = `/auth/silent-login?email=${userCreateSuccess.email}&refreshToken=LONGCHARACTERERBUTSTILLINVALIDTOPASSDETECTIONNOW`;
    const response = await request(baseUrl).get(url);
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/silent-login (GET) - should error when email doesnt match toekn payload', async () => {
    const url = `/auth/silent-login?email=${
      userCreateSuccess.email + 'not'
    }&refreshToken=${tokenResponse.refresh_token}`;
    const response = await request(baseUrl).get(url);
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/auth/logout (GET) - should error when not loggued in', async () => {
    const url = `/auth/logout`;
    const response = await request(baseUrl).get(url).send();
    expect(response.status).toBe(HttpStatus.FORBIDDEN);
    return response;
  });

  it('/auth/logout (GET) - should succeed with lgout', async () => {
    const url = `/auth/logout`;
    const response = await request(baseUrl)
      .get(url)
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send();
    expect(response.status).toBe(HttpStatus.OK);
    return response;
  });
});
