import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app/app.module';
import { IRoom } from '@bookme/booklib';
import { ConfigService } from '@nestjs/config';

describe('Rooms (e2e)', () => {
  const config = new ConfigService();
  const port = config.get("GATEWAY_SERVICE_PORT") || process.env.PORT || 3334;
  const baseUrl = `http://localhost:${port}`;

  beforeEach(async () => {
    return true;
  });

  afterEach(async() => {
    return true;
  })

  it('/rooms (GET) - should list the rooms', async () => {
    const response = await request(baseUrl).get('/rooms');
    expect(response.status).toBe(HttpStatus.OK);

    return response;
  })

  it('/rooms (GET) - should find a Mush room', async () => {
    const response = await request(baseUrl).get('/rooms');
    const rooms:IRoom[] = response.body.rooms;
    const mushRoom = rooms?.find( (room) => room.name=="Mush room" );
    // console.log(`response.body`, response.body);
    expect(response.status).toBe(HttpStatus.OK);
    expect(mushRoom).toBeDefined();
    // console.log(mushRoom);
    return response;
  })

});