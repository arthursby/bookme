import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app/app.module';
import { Client } from 'pg';
import { dbCredentials } from './common';
import * as bcrypt from 'bcrypt';
import {
  demoUser,
  demoUser2,
  ILogin,
  IReservation,
  ITokenResponse,
  VerifyReservationDto,
} from '@bookme/booklib';
import { createReservationSuccess } from '@bookme/services/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { addMinutes, parseISO, subMinutes } from 'date-fns';
import { start } from 'repl';
import { CompressionTypes } from '@nestjs/microservices/external/kafka.interface';

describe('Reservations (e2e)', () => {
  let app: INestApplication;
  let dbClient: Client;
  let tokenResponse: ITokenResponse;
  let createdReservation: IReservation;
  const config = new ConfigService();
  const port = config.get('GATEWAY_SERVICE_PORT') || process.env.PORT || 3334;
  const baseUrl = `http://localhost:${port}`;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.init();

    // dbClient = new Client(dbCredentials);
    // await dbClient.connect();
    // await resetDB();
    const setupResponse = await request(baseUrl).get(`/setup`);
    tokenResponse = await getLoginToken(demoUser);
    // console.log(`setupResponse`, setupResponse);
  });

  const resetDB = async () => {
    await dbClient.query('truncate "user" cascade');
    await dbClient.query('truncate "token" cascade');
    await dbClient.query('truncate "reservation" cascade');
    // const password = await bcrypt.hash(demoUser.password, 10);
    // await dbClient.query(
    //   `insert into "user" (email, password) values ('${demoUser.password}','${password}')`
    // );
  };

  const getLoginToken = async (loginDto: ILogin) => {
    const response = await request(baseUrl)
      .post('/auth/login')
      .send(loginDto);
    const tokenResponse = response.body;
    return tokenResponse;
  };

  beforeEach(async () => {
    return true;
  });

  afterEach(async () => {
    return true;
  });

  afterAll(async () => {
    // dbClient.end();
  });

  it('/reservations (GET) - should OK without login', async () => {
    const response = await request(baseUrl).get('/reservations');
    expect(response.status).toBe(HttpStatus.OK);
    // setup should have created 5 reservatoions
    console.log('result GET', response.body);
    expect(response.body.total).toBe(5);
    return response;
  });

  it('/reservations (POST) - should error when not logged in ', async () => {
    const url = `/reservations`;
    const response = await request(baseUrl).post(url).send();
    expect(response.status).toBe(HttpStatus.FORBIDDEN);
    return response;
  });

  it('/reservations (POST) - should error when missing data ', async () => {
    const url = `/reservations`;
    const response = await request(baseUrl)
      .post(url)
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send();

    expect(response.status).toBe(HttpStatus.BAD_REQUEST);

    return response;
  });

  it('/reservations (POST) - should create a reservation ', async () => {
    const url = `/reservations`;

    const response = await request(baseUrl)
      .post(url)
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send({...createReservationSuccess, userId: tokenResponse.user.id});
    expect(response.status).toBe(HttpStatus.CREATED);
    createdReservation = response.body;
    // console.info(response);
    expect(createdReservation.title).toBe(createReservationSuccess.title);

    return response;
  });

  it('/reservations (POST) - title cannot be empty ', async () => {
    const url = `/reservations`;

    const response = await request(baseUrl)
      .post(url)
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send({ ...createReservationSuccess, title: '' });
    expect(response.status).toBe(HttpStatus.BAD_REQUEST);
    return response;
  });

  it('/reservations (GET) - should find created reservation', async () => {
    const response = await request(baseUrl).get(
      `/reservations?roomIds[0]=${createdReservation.roomId}`
    );
    expect(response.status).toBe(HttpStatus.OK);

    // console.log(`response`, response.body.data);
    const reservations: IReservation[] = response.body.data;
    const found = reservations.find((res) => res.id == createdReservation?.id);
    expect(found).toBeDefined();

    return response;
  });

  it('/reservations/verify (POST) - should detect overlapping reservations', async () => {
    /**
     *      [+++++++++++]           10-11am
     *    [------]                  A 9:50am-10:20am  BAD
     *              [----]          B 10:50am-11:10am BAD
     *        [----]                C 10:20am-10:40am BAD
     *   [-----------------]        D 9:50am-11:30am  BAD
     * [--]                         E 9:30am-9:55AM   GOOD
     *                    [----]    F 11:10am-11:30am GOOD
     *
     */
    // const newBaseUrl = "http://localhost:5559";
    const newBaseUrl = baseUrl + '/reservations';
    const base: VerifyReservationDto = {
      roomId: createdReservation.roomId,
      start: new Date(createdReservation.start),
      end: new Date(createdReservation.end),
    };
    const testA = { ...base };
    testA.start = subMinutes(testA.start, 10);
    testA.end = addMinutes(testA.end, 40);

    const testB = { ...base };
    testB.start = addMinutes(testB.start, 50);
    testB.end = addMinutes(testB.end, 10);

    const testC = { ...base };
    testC.start = addMinutes(testC.start, 20);
    testC.end = subMinutes(testC.end, 20);

    const testD = { ...base };
    testD.start = subMinutes(testD.start, 10);
    testD.end = addMinutes(testD.end, 30);

    const testE = { ...base };
    testE.start = subMinutes(testE.start, 30);
    testE.end = subMinutes(testE.end, 65);

    const testF = { ...base };
    testF.start = addMinutes(testF.start, 70);
    testF.end = addMinutes(testF.end, 30);

    let response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testA);
    console.log(`testA:`, response.body);
    expect(response.body.count).toBeGreaterThanOrEqual(1);

    response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testB);
    console.log(`testB:`, response.body);
    expect(response.body.count).toBeGreaterThanOrEqual(1);

    response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testC);
    console.log(`testC:`, response.body);
    expect(response.body.count).toBeGreaterThanOrEqual(1);

    response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testD);
    console.log(`testD:`, response.body);
    expect(response.body.count).toBeGreaterThanOrEqual(1);

    response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testE);
    console.log(`testE:`, testE, response.body);
    expect(response.body.count).toBe(0);

    response = await request(newBaseUrl)
      .post('/verify')
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send(testF);
    console.log(`testF:`, testF, response.body);
    expect(response.body.count).toBe(0);
  });

  it('/reservations (DELETE) - should error without valid bearer token ', async () => {
     // delete
     const response = await request(baseUrl)
     .delete(`/reservations/${createdReservation.id}`)
    //  .set('Authorization', 'Bearer ' + tokenResponse.access_token)
     .send();
   expect(response.status).toBe(HttpStatus.FORBIDDEN);

   return response;
  }) 

  it('/reservations (DELETE) - should fail to delete when not owner ', async () => {
    // use a different bearer token.
    const tokenReponseDemo2:ITokenResponse = await getLoginToken(demoUser2);

    // delete
    const response = await request(baseUrl)
    .delete(`/reservations/${createdReservation.id}`)
    .set('Authorization', 'Bearer ' + tokenReponseDemo2.access_token)
    .send();
  expect(response.status).toBe(HttpStatus.FORBIDDEN);

  return response;
 }) 

  it('/reservations (DELETE) - should delete the created reservation', async () => {
    // count before
    const countBeforeResponse = await request(baseUrl).get('/reservations');
    const total = countBeforeResponse.body.total;

    // delete
    const response = await request(baseUrl)
      .delete(`/reservations/${createdReservation.id}`)
      .set('Authorization', 'Bearer ' + tokenResponse.access_token)
      .send();
    expect(response.status).toBe(HttpStatus.OK);

    // count after should be total-1 or 0.
    const expectedNewTotal = total > 0 ? total - 1 : 0;
    const countAfterResponse = await request(baseUrl).get('/reservations');
    const newTotal = countAfterResponse.body.total;
    expect(newTotal).toBe(expectedNewTotal);

    return response;
  });
});
