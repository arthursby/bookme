import {
  CreateReservationDto,
  FindReservationDto,
  IReservation,
  IRoom,
  VerifyReservationDto,
  VerifyReservationResultDto,
} from '@bookme/booklib';
import { bookmeApi } from './bookme-api';

export interface QueryResult {
  data: IReservation[];
  total: number;
  current: number;
}

export interface IQueryReservationsFilter {
  query?: string;
  rooms: IRoom[];
}

const reservationsApi = bookmeApi.injectEndpoints({
  overrideExisting: true,
  endpoints: (builder) => ({
    getReservations: builder.query<QueryResult, FindReservationDto>({
      query: (filter) => {
        let params = `end=${encodeURIComponent(
          filter.end as string
        )}&temp=1&start=${encodeURIComponent(filter.start as string)}&`;
        filter.roomIds.forEach((roomId, index) => {
          params += `roomIds[${index + 1}]=${roomId}&`;
        });

        // console.debug(`params`,params);
        return `reservations?${params}`;
      },
      providesTags: (result) => {
        if (result && result.data) {
          return result.data.map(({ id }) => ({
            type: 'reservations' as const,
            _id: id,
          }));
        } else {
          return [{ type: 'reservations', _id: 'LIST' }];
        }
      },
    }),
    verify: builder.mutation<VerifyReservationResultDto, VerifyReservationDto>({
      query: (verifyParams) => ({
        url: `reservations/verify`,
        method: 'POST',
        body: verifyParams,
      }),
    }),
    getReservation: builder.query<IReservation, string>({
      query: (id) => `reservations/${id}`,
      providesTags: (result, error, arg) => [
        { type: 'reservations', _id: arg },
      ],
    }),
    createReservation: builder.mutation<IReservation, CreateReservationDto>({
      query: (aeservation) => ({
        url: `reservations`,
        method: 'POST',
        body: aeservation,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'reservations', _id: arg },
      ],
    }),
    updateReservation: builder.mutation<IReservation, IReservation>({
      query: (reservation) => ({
        url: `reservations/${reservation.id}`,
        method: 'PATCH',
        body: reservation,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'reservations', _id: arg },
      ],
    }),
    removeReservation: builder.mutation<void, number>({
      query: (id) => {
        return {
          url: `reservations/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: 'reservations', id: arg },
      ],
    }),
  }),
});

export const {
  useGetReservationsQuery,
  useCreateReservationMutation,
  useUpdateReservationMutation,
  useRemoveReservationMutation,
  useVerifyMutation,
} = reservationsApi;
