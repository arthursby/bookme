import { FindReservationDto, IRoom } from '@bookme/booklib';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { add } from 'date-fns';

type SelectorType = {
  rooms: IRoom[];
  query: FindReservationDto;
};

const initialState: SelectorType = {
  rooms: [],
  query: {
    roomIds: [],
    // start: new Date(),
    // start: new Date().toLocaleDateString(),
    start: new Date().toLocaleDateString(),
    end: add(new Date(), { months: 1 }).toLocaleDateString(),
  },
};

export const selectorSlice = createSlice({
  name: 'Selector',
  initialState,
  reducers: {
    selectRooms: (state, action: PayloadAction<IRoom[]>) => {
      const roomIds = action.payload.map((r) => r.id);
      state.query.roomIds = roomIds;
      state.rooms = action.payload;
    },
    setQueryDates: (state, action: PayloadAction<string>) => {
      // Parse scheduler query '?start=SSSSS&nd=EEEEEE'
      const params = new URLSearchParams(action.payload);
      const start = params.get('start');
      const end = params.get('end');
      // console.debug(`set query dates [${action.payload}]`, typeof start, typeof end);
      state.query.start = start;
      state.query.end = end;
    },
  },
});

// Action creators are generated for each case reducer function
export const { selectRooms, setQueryDates } = selectorSlice.actions;

export const selectorReducer = selectorSlice.reducer;
