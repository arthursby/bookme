import { IRoomList } from '@bookme/booklib';
import { bookmeApi } from './bookme-api';

const roomsApi = bookmeApi.injectEndpoints({
  overrideExisting: true,
  endpoints: (builder) => ({
    getRooms: builder.query<IRoomList, void>({
      query: () => `rooms`,
      providesTags: (result?: IRoomList) => {
        if (!result) return [];
        const rooms = result.rooms || [];
        return [
          { type: 'rooms', _id: 'LIST' },
          ...rooms.map(({ id }) => ({
            type: 'rooms' as const,
            _id: id,
          })),
        ];
      },
    }),
  }),
});

export const { useGetRoomsQuery } = roomsApi;
