import { ILogin, ITokenResponse, SilentLoginDto } from '@bookme/booklib';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { STORAGE_KEY } from '../config/site.config';
import { bookmeApi } from './bookme-api';

export interface UserCredentials {
  email: string;
  password: string;
}

export interface AuthState {
  authenticated: boolean;
  auth?: ITokenResponse;
}

const authApi = bookmeApi.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<ITokenResponse, ILogin>({
      query: (body) => ({
        url: `/auth/login`,
        method: 'POST',
        body: body,
      }),
    }),
    refresh: builder.query<ITokenResponse, SilentLoginDto>({
      query: (refreshTokenInfo) => ({
        url: `/auth/silent-login?refreshToken=${refreshTokenInfo.refreshToken}&email=${refreshTokenInfo.email}`,
        headers: { authorization: '' },
      }),
    }),
    logout: builder.query<void, void>({
      query: () => `/auth/logout`,
    }),
  }),
});

export const { useLoginMutation, useLazyLogoutQuery, useLazyRefreshQuery } =
  authApi;

const authInitState: AuthState = { authenticated: false };

export const authSlice = createSlice({
  name: 'Auth',
  initialState: authInitState,
  reducers: {
    onLogin: (state, action: PayloadAction<ITokenResponse>) => {
      const payload = action.payload;
      if (!action.payload) {
        console.warn(`shouldn't happen, undefined payload in onLogin`);
        return;
      }

      let { expires_at, refresh_token_expires_at } = payload;
      if (!expires_at) {
        // Create expires_at from expires_in
        // Skip creation if fields already available to avoid overwriting actual expiration time
        const now = new Date().getTime();
        expires_at = now + payload.expires_in * 1000;
        refresh_token_expires_at =
          now + payload.refresh_token_expires_in * 1000;
      }

      state.authenticated = true;
      state.auth = {
        ...state.auth,
        ...payload, // overwrite with payload
        expires_at,
        refresh_token_expires_at,
      };
      console.debug(`New login token created`, state.auth);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(state.auth));
    },
    onLogout: (state) => {
      state.authenticated = false;
      state.auth = undefined;
      localStorage.removeItem(STORAGE_KEY);
    },
  },
});

// Action creators are generated for each case reducer function
export const { onLogin, onLogout } = authSlice.actions;

export const authReducer = authSlice.reducer;
