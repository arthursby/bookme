import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { siteConfig } from '../config/site.config';
import { RootState } from '../context/store';

export const bookmeApi = createApi({
  reducerPath: 'bookmeApi',
  tagTypes: ['reservations', 'rooms'],
  baseQuery: fetchBaseQuery({
    baseUrl: siteConfig.apiUrl,
    prepareHeaders: (headers, { getState }) => {
      const state = getState() as RootState;
      const token = state.Auth.auth?.access_token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    fullReset: builder.mutation<void, void>({
      query: () => ({
        url: `/reset`,
        method: 'GET',
      }),
      invalidatesTags: () => {
        return [{ type: 'reservations', _id: 'LIST' }];
      },
    }),
    setup: builder.mutation<void, void>({
      query: () => ({
        url: `/setup`,
        method: 'GET',
      }),
      invalidatesTags: () => {
        return [{ type: 'reservations', _id: 'LIST' }];
      },
    }),
  }),
});

export const { useFullResetMutation, useSetupMutation } = bookmeApi;
