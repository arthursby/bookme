import {
  Action,
  AnyAction,
  combineReducers,
  configureStore,
  ThunkAction,
} from '@reduxjs/toolkit';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import { authReducer } from '../features/auth.slice';
import { bookmeApi } from '../features/bookme-api';
import { selectorReducer } from '../features/selector.slice';

const allReducers = {
  Selector: selectorReducer,
  Auth: authReducer,
  [bookmeApi.reducerPath]: bookmeApi.reducer,
};
const combinedReducer = combineReducers(allReducers);

const reducer: typeof combinedReducer = (state, action: AnyAction) => {
  if (action.type === HYDRATE) {
    console.log(`reducer action ${action.type}`, action);
    const nextState = {
      ...state, // use previous state
      ...action['payload'], // apply delta from hydration
    };
    return nextState;
  } else {
    return combinedReducer(state, action);
  }
};

export const store = configureStore({
  reducer: reducer,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(bookmeApi.middleware);
  },
});

export const initStore = () => {
  return store;
};

type Store = ReturnType<typeof initStore>;

export type AppDispatch = Store['dispatch'];
export type RootState = ReturnType<Store['getState']>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const wrapper = createWrapper(initStore, {
  debug: process.env.NODE_ENV === 'development',
});
