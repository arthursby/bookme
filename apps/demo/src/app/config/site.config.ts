import { environment } from '../../environments/environment';

export const STORAGE_KEY = 'auth';

// console.debug(`site env`, process.env);
export const siteConfig = {
  siteName: 'BOOKME WebApp',
  siteIcon: '',
  footerText: `BookMe @ ${new Date().getFullYear()} `,
  enableAnimatedRoute: false,
  apiUrl: environment.apiUrl || '/api',
  google: {
    analyticsKey: 'UA-xxxxxxxxx-1',
  },
};
