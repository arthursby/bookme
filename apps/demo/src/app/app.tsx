// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Scheduler } from '@aldabil/react-scheduler';
import { IReservation } from '@bookme/booklib';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import { useSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { PageHeader } from './components/Header';
import { ReservationEditor } from './components/reservation-editor';
import { RoomsSelector } from './components/room-selector';
import { STORAGE_KEY } from './config/site.config';
import { useAppDispatch, useAppSelector } from './context/hooks';
import { onLogin, useLazyRefreshQuery } from './features/auth.slice';
import {
  QueryResult,
  useGetReservationsQuery,
  useRemoveReservationMutation,
} from './features/reservations.slice';
import { setQueryDates } from './features/selector.slice';

// needs to use this pageScope as a hack because somehow the components variable aren't available within the scheduler context.
const pageScope = {
  auth: undefined,
  data: undefined,
  query: undefined,
};

const App = () => {
  const dispatch = useAppDispatch();
  const { authenticated, auth } = useAppSelector((state) => state.Auth);
  const email = auth?.user?.email;
  const [ready, setReady] = useState(false);
  const [refreshLogin] = useLazyRefreshQuery();
  const query = useAppSelector((state) => state.Selector.query);
  const selectedRooms = useAppSelector((state) => state.Selector.rooms);
  // const [queryParams, setQueryParams] = useState<IQueryReservationsFilter>({ rooms: selectedRooms });

  const { data, isLoading, isFetching, isSuccess, error, refetch } =
    useGetReservationsQuery(query);
  const { enqueueSnackbar } = useSnackbar();
  const [triggerRemove, removeResult] = useRemoveReservationMutation();

  useEffect(() => {
    // console.debug(`selected Rooms`, selectedRooms);
    pageScope.query = query;
    pageScope.auth = auth;
    pageScope.data = data;
  }, [query, data, auth]);

  const formatForScheduler = (paginatedResult?: QueryResult) => {
    if (!paginatedResult) return [];

    const data = paginatedResult.data || [];
    const res = data.map((ev) => ({
      ...ev,
      event_id: ev.id,
      start: new Date(ev.start),
      end: new Date(ev.end),
    }));
    // console.debug(`see result`, res);
    return res;
  };

  const formatRoomsForScheduler = () => {
    return selectedRooms.map((r) => ({ id: r.id, value: r.id, text: r.name }));
  };

  useEffect(() => {
    const auth = localStorage.getItem(STORAGE_KEY);
    let interval: ReturnType<typeof setTimeout>;

    if (auth) {
      const authData = JSON.parse(auth);
      console.debug(`auto login`, authData);
      // is it expired?
      const now = new Date();
      const expiresAt = new Date(authData.expires_at);
      const refreshExpiresAt = new Date(authData.refresh_token_expires_at);

      const tokenExpired = expiresAt < now;
      const refreshExpired = refreshExpiresAt < now;
      console.debug(`token expired=${tokenExpired}`, expiresAt);
      console.debug(`refresh expired=${refreshExpired}`, refreshExpiresAt);
      if (!tokenExpired) {
        dispatch(onLogin(authData));
        setReady(true);
      } else if (!refreshExpired) {
        const silentLogin = async () => {
          // console.debug(`silent login now`);
          const authResult = await refreshLogin({
            refreshToken: authData.refresh_token,
            email: authData.user.email,
          });
          if (!authResult.isError && authResult.data) {
            dispatch(onLogin(authResult.data));
          } else {
            console.warn(
              `silent login error - credentials have expired`,
              authResult
            );
            enqueueSnackbar('Login credentials have expired', {
              variant: 'warning',
            });
          }
          setReady(true);
        };
        silentLogin();
        // Every 10min before access_token expiration (every 30min)
        interval = setInterval(silentLogin, 10 * 60 * 1000);
      }
    } else {
      setReady(true);
    }
    return () => clearInterval(interval);
  }, [dispatch, refreshLogin, enqueueSnackbar]);

  const handleDelete = async (selectedId) => {
    // console.debug( `handleDelete selectedId=${selectedId}`, pageScope);
    const { auth, data } = pageScope;
    // console.debug(`data is`, data);
    if (!auth?.user) {
      enqueueSnackbar('Permission denied - Please login first.', {
        variant: 'warning',
      });
      return Promise.reject(`permission denied`);
    }
    const found = data.data.find(
      (resa: IReservation) => resa.id === selectedId
    );
    if (!found) {
      return Promise.reject(`not found`);
    }
    if (found.userId !== auth.user.id) {
      enqueueSnackbar('Can only remove your own events', {
        variant: 'error',
      });
      return Promise.reject(`Permission Denied`);
    }

    try {
      await triggerRemove(parseInt(selectedId)).unwrap();
      refetch();
      return Promise.resolve(selectedId);
    } catch (err) {
      enqueueSnackbar('DELETE not implemented', { variant: 'warning' });
      // console.debug(`an error occured`, err);
      return Promise.reject('error');
    }
  };

  return (
    <div>
      <PageHeader />
      <input type="hidden" id="total" value={data?.total} />
      <RoomsSelector filter={{}} />
      <Accordion>
        <AccordionSummary>Query</AccordionSummary>
        <AccordionDetails>
          <pre>{JSON.stringify(query, null, 2)}</pre>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary>Results</AccordionSummary>
        <AccordionDetails>
          <pre>{JSON.stringify(data, null, 2)}</pre>
        </AccordionDetails>
      </Accordion>
      <Scheduler
        view="month"
        loading={isLoading || isFetching}
        month={{
          weekDays: [0, 1, 2, 3, 4, 5, 6],
          weekStartOn: 0,
          startHour: 0,
          endHour: 23,
        }}
        week={{
          weekDays: [0, 1, 2, 3, 4, 5, 6],
          weekStartOn: 6,
          startHour: 0,
          endHour: 23,
          step: 60,
        }}
        day={{
          startHour: 0,
          endHour: 23,
          step: 60,
        }}
        fields={[
          {
            name: 'roomId',
            type: 'select',
            options: formatRoomsForScheduler(),
            config: { label: 'Room', required: true },
          },
        ]}
        onDelete={handleDelete}
        customEditor={(scheduler) => {
          // console.debug(`customEditor`, auth, pageScope);
          return <ReservationEditor scheduler={scheduler} />;
        }}
        // recourseHeaderComponent={() => {
        //   return <div>New Header?</div>;
        // }}
        events={formatForScheduler(data)}
        viewerTitleComponent={(event) => <span>{event.title}</span>}
        remoteEvents={async (query) => {
          dispatch(setQueryDates(query));
          return Promise.resolve([]);
        }}
        selectedDate={new Date()}
      />
    </div>
  );
};

export default App;
