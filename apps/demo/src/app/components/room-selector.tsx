import { IRoom } from '@bookme/booklib';
import { Box, CircularProgress } from '@mui/material';
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../context/hooks';
import { useGetRoomsQuery } from '../features/rooms.slice';
import { selectRooms } from '../features/selector.slice';

interface IRoomsSelectorProps {
  filter: unknown;
}

export const RoomsSelector = (props: IRoomsSelectorProps) => {
  const dispatch = useAppDispatch();
  const selectedRooms = useAppSelector((state) => state.Selector.rooms);
  const { data, isLoading, isSuccess, isError } = useGetRoomsQuery();

  useEffect(() => {
    if (isSuccess && data) {
      // select all rooms by default
      dispatch(selectRooms(data.rooms));
    }
  }, [data, isSuccess, dispatch]);

  const toggleSelection = (room: IRoom, index: number) => {
    const selection = [...selectedRooms];

    if (index !== -1) {
      // remove from selection
      selection.splice(index, 1);
    } else {
      // add to selection
      selection.push(room);
    }
    dispatch(selectRooms(selection));
  };

  if (isLoading)
    return (
      <Box sx={{ display: 'flex' }}>
        <CircularProgress />
      </Box>
    );

  if (isError) {
    return <div>An error occured</div>;
  }

  return (
    <div>
      {/* {JSON.stringify(data)} */}
      {data &&
        data.rooms.map((room, index) => {
          const roomIndex = selectedRooms.findIndex((r) => r.id === room.id);
          const selected = roomIndex !== -1;
          // console.debug(`room${room.name} selected=${selected}`, selectedRooms);
          return (
            <button
              key={`key_r${index}`}
              className={`room-selector ${selected ? 'selected' : ''}`}
              style={{
                margin: 5,
                padding: 5,
                width: 100,
                backgroundColor: selected ? room.color : '',
              }}
              onClick={(e) => toggleSelection(room, roomIndex)}
            >
              {room.name}
            </button>
          );
        })}
    </div>
  );
};
