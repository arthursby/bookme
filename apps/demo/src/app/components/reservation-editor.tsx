import {
  ProcessedEvent,
  SchedulerHelpers,
} from '@aldabil/react-scheduler/dist/types';
import { CreateReservationDto, IReservation } from '@bookme/booklib';
import { DateTimePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LoadingButton from '@mui/lab/LoadingButton';
import {
  Alert,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  useMediaQuery,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { add } from 'date-fns';
import { useSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { useAppSelector } from '../context/hooks';
import {
  useCreateReservationMutation,
  useVerifyMutation,
} from '../features/reservations.slice';

interface ICustomEditorProps {
  scheduler: SchedulerHelpers;
}

interface IEditorErrors {
  title?: string;
  end?: string;
  start?: string;
  roomId?: string;
  helper?: string;
  count: number;
}

export const ReservationEditor = ({ scheduler }: ICustomEditorProps) => {
  const event = scheduler.edited;
  const { authenticated, auth } = useAppSelector((state) => state.Auth);
  const selectedRooms = useAppSelector((state) => state.Selector.rooms);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const [verify, verifyResult] = useVerifyMutation();
  const [create, createResult] = useCreateReservationMutation();
  const [isValid, setIsValid] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  // Make your own form/state
  const [state, setState] = useState<Partial<IReservation>>({
    title: event?.title || '',
    start: event?.start || new Date(),
    end: event?.end || add(new Date(), { hours: 1 }),
    roomId: event && event.roomId ? event.roomId : '',
  });
  const [error, setError] = useState<IEditorErrors>({ count: 0 });

  useEffect(() => {
    validateForm();
  }, [state]);

  useEffect(() => {
    // console.debug(`create editor`, scheduler);
  }, []);

  const handleChange = (value: string | Date, name: string) => {
    // console.debug(`hnandle change`, value, name);
    setState((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const validateForm = async () => {
    // console.debug(`[validateForm] state is`, state);
    setIsValid(false);
    let errorsCount = 0;
    const errors: IEditorErrors = { count: 0 };
    if (state.title.length < 3) {
      errors.title = 'Min 3 letters';
      errorsCount++;
    }

    const sRoom = state.roomId as unknown as string;
    // console.debug(`sRoom=${sRoom} && roomId=${state.roomId}`);
    if (!state.roomId || sRoom === '') {
      errors.roomId = 'A room is required';
      errorsCount++;
    }

    if (!state.start) {
      errors.start = 'Start date required';
      errorsCount++;
    }

    if (!state.start) {
      errors.end = 'End date required';
      errorsCount++;
    }

    if (state.end < state.start) {
      errors.end = 'Invalid end date';
      errorsCount++;
    }

    // console.debug(`[validateForm] errors [count=${errorsCount}] are `, errors);

    errors.count = errorsCount;
    setError(errors);

    if (errorsCount > 0) {
      return errorsCount;
    }

    try {
      const result = await verify({
        end: state.end,
        start: state.start,
        roomId: state.roomId,
      }).unwrap();
      const overlaps = result.count;
      // console.debug(`overlaps: `, result, overlaps);
      if (overlaps === 0) {
        setIsValid(true);
      }
    } catch (err) {
      console.error(`something wrong happened`, err);
    }

    return errorsCount;
  };

  const handleSubmit = async () => {
    try {
      const errors = await validateForm();
      if (errors !== 0) {
        return;
      }

      scheduler.loading(true);

      const reservationDto: CreateReservationDto = {
        roomId: state.roomId,
        start: state.start,
        end: state.end,
        title: state.title,
        userId: auth.user.id,
        color: '',
      };
      const res = await create(reservationDto).unwrap();
      // console.debug(`create result`, res);
      enqueueSnackbar('Event created.', { variant: 'success' });

      scheduler.close();
    } catch (err) {
      console.error(`ooopsie`, error);
    } finally {
      scheduler.loading(false);
    }
  };
  return (
    <Dialog open={true} fullScreen={isMobile} id="editor">
      <DialogTitle>{event ? 'Edit' : 'Add'} Event</DialogTitle>
      <DialogContent style={{ padding: '10px' }}>
        {!authenticated ? (
          <div style={{ padding: '20px' }}>
            <Alert severity="error">
              You must be logged in to create an avent.
            </Alert>
          </div>
        ) : event ? (
          <div style={{ padding: '20px' }}>
            <Alert severity="warning">Edit feature not implemented</Alert>
          </div>
        ) : (
          <Grid container spacing={1}>
            <Grid item sm={12} xs={12}>
              <TextField
                label="Title"
                id="editor-title"
                value={state.title}
                onChange={(e) => handleChange(e.target.value, 'title')}
                error={error?.title !== undefined}
                helperText={error?.title !== undefined && error['title']}
                fullWidth
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  label="Start"
                  value={state.start}
                  minutesStep={5}
                  onChange={(e) => handleChange(new Date(e || ''), 'start')}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ width: '100%' }}
                      error={error?.start !== undefined}
                      helperText={error?.start && error['start']}
                    />
                  )}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item sm={6} xs={12}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  label="End"
                  value={state.end}
                  minutesStep={5}
                  onChange={(e) => handleChange(new Date(e || ''), 'end')}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ width: '100%' }}
                      error={error?.end !== undefined}
                      helperText={error?.end && error['end']}
                    />
                  )}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item sm={12} xs={12}>
              <FormControl fullWidth error={error?.roomId !== undefined}>
                <InputLabel id="demo-simple-select-label">Room</InputLabel>
                <Select
                  labelId="room-select-label"
                  id="editor-room"
                  label="Room"
                  value={state.roomId}
                  onChange={(e) =>
                    handleChange(e.target.value as string, 'roomId')
                  }
                >
                  {selectedRooms.map((room, index) => (
                    <MenuItem key={`room_${index}`} value={room.id}>
                      {room.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormHelperText>
                error: {JSON.stringify(error)}
                <br />
                isValid: {JSON.stringify(isValid)}
                <br />
                {JSON.stringify(verifyResult.data)}
                {/* <pre>{JSON.stringify(verifyResult, null, 2)}</pre> */}
                {verifyResult.isLoading && <CircularProgress />}
                {verifyResult.isSuccess && !isValid && (
                  <Alert severity="error">
                    The room is not available for these dates.
                  </Alert>
                )}
                {isValid && (
                  <Alert id="room-available" severity="success">
                    The room is available
                  </Alert>
                )}
              </FormHelperText>
            </Grid>
          </Grid>
        )}
        <div>
          {createResult.isError && (
            <Alert severity="error">An error occured.</Alert>
          )}
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={scheduler.close} id="editor-cancel-btn">
          Cancel
        </Button>
        {authenticated && !event && (
          <LoadingButton
            disabled={error?.count !== 0 || createResult.isLoading || !isValid}
            loading={verifyResult.isLoading || createResult.isLoading}
            id="editor-confirm-btn"
            onClick={handleSubmit}
          >
            Confirm
          </LoadingButton>
        )}
      </DialogActions>
    </Dialog>
  );
};
