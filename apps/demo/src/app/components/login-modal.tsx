import { LoadingButton } from '@mui/lab';
import {
  Alert,
  AlertTitle,
  Dialog,
  DialogActions,
  Grid,
  TextField,
} from '@mui/material';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Fade from '@mui/material/Fade';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useAppDispatch } from '../context/hooks';
import { onLogin, useLoginMutation } from '../features/auth.slice';

export const LoginModal = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const dispatch = useAppDispatch();
  const [login, loginResult] = useLoginMutation();
  const { enqueueSnackbar } = useSnackbar();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const email = formData.get('email') as string;
    const password = formData.get('password') as string;

    try {
      // Send them back to the page they tried to visit when they were
      // redirected to the login page. Use { replace: true } so we don't create
      // another entry in the history stack for the login page.  This means that
      // when they get to the protected page and click the back button, they
      // won't end up back on the login page, which is also really nice for the
      // user experience.
      const response = await login({ email, password }).unwrap();
      console.debug(`logn response is `, response);
      dispatch(onLogin(response));
      setOpen(false);
      enqueueSnackbar('Login success', { variant: 'success' });
    } catch (error) {
      // Login error
      console.warn(`couldn't login`, error);
    }
  };

  return (
    <>
      <Button id="login-btn" onClick={handleOpen}>
        Login now
      </Button>
      <Dialog
        open={open}
        // fullScreen={isMobile}
        onClose={handleClose}
        maxWidth={'sm'}
        id="login-dialog"
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box id="login-box">
            <form
              className="form"
              onSubmit={handleSubmit}
              style={{ padding: 15 }}
            >
              <div style={{ margin: '15px' }}>
                email: demo@consensys.com
                <br />
                password: Findag00dpassw0rd
              </div>
              <Grid container spacing={1}>
                <Grid item sm={12} xs={12}>
                  <TextField
                    id="email"
                    value="demo@consensys.com"
                    name="email"
                    label="Email"
                    variant="outlined"
                    style={{ width: '100%' }}
                    required
                  />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <TextField
                    id="password"
                    value="Findag00dpassw0rd"
                    name="password"
                    label="Password"
                    variant="outlined"
                    type="password"
                    style={{ width: '100%' }}
                    required
                  />
                </Grid>
              </Grid>
              {loginResult.isError && (
                <div style={{ margin: 5, padding: 5 }}>
                  <Alert severity="error">
                    <AlertTitle id="alert-error-cred">
                      Invalid Credentials
                    </AlertTitle>
                    Did you run 'APP SETUP'?
                  </Alert>
                </div>
              )}

              <DialogActions>
                <Button id="cancel" onClick={handleClose}>
                  Cancel
                </Button>
                <LoadingButton
                  type="submit"
                  id="login-submit"
                  color="primary"
                  className="form__custom-button"
                  loading={loginResult.isLoading}
                >
                  Log in
                </LoadingButton>
              </DialogActions>
            </form>
          </Box>
        </Fade>
      </Dialog>
    </>
  );
};
