import { IReservation } from '@bookme/booklib';

type EventViewerProps = {
  event: IReservation;
};

export const EventViewer = ({ event }: EventViewerProps) => {
  return (
    <div className={`p-5`} style={{ backgroundColor: event.color }}>
      <div className="content-section">
        <label>From:</label>
        <div className="date-start">{event.start.toLocaleDateString()}</div>
      </div>
      <div className="content-section">
        <label>To:</label>
        <div className="date-end">{event.end.toLocaleDateString()}</div>
      </div>
      <div className="content-section">
        <label>Label:</label>
        <div className="label">{event.title}</div>
      </div>
    </div>
  );
};
