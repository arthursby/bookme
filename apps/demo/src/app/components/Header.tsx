import { Button } from '@mui/material';
import { useSnackbar } from 'notistack';
import { SyntheticEvent } from 'react';
import { useAppDispatch, useAppSelector } from '../context/hooks';
import { onLogout, useLazyLogoutQuery } from '../features/auth.slice';
import { useFullResetMutation, useSetupMutation } from '../features/bookme-api';
import { useGetReservationsQuery } from '../features/reservations.slice';
import { useGetRoomsQuery } from '../features/rooms.slice';
import { selectRooms } from '../features/selector.slice';
import { LoginModal } from './login-modal';

export const PageHeader = () => {
  const { authenticated, auth } = useAppSelector((state) => state.Auth);
  const { enqueueSnackbar } = useSnackbar();
  const [logout] = useLazyLogoutQuery();
  const dispatch = useAppDispatch();
  const [triggerReset] = useFullResetMutation();
  const [triggerSetup] = useSetupMutation();
  const query = useAppSelector((state) => state.Selector.query);
  const reservationQuery = useGetReservationsQuery(query);
  const selectedRooms = useAppSelector((state) => state.Selector.rooms);
  const roomsQuery = useGetRoomsQuery();
  const email = auth?.user?.email;

  const handleLogout = (e?: SyntheticEvent) => {
    if (e) {
      e.preventDefault();
    }
    if (authenticated) {
      logout();
      dispatch(onLogout());
      enqueueSnackbar('You have been logged out.', { variant: 'info' });
    }
  };

  const handleReset = async () => {
    await triggerReset().unwrap();
    handleLogout();
  };

  const handleSetup = async () => {
    await triggerSetup().unwrap();

    roomsQuery.refetch();

    handleLogout();
    enqueueSnackbar('Setup has been successful.', { variant: 'success' });

    // refetch reservations
    setTimeout(() => {
      reservationQuery.refetch();
    }, 1000);
  };

  return (
    <div id="header">
      {authenticated && (
        <>
          {email}{' '}
          <a id="logout-btn" href="#logout" onClick={handleLogout}>
            logout
          </a>
        </>
      )}
      {!authenticated && <LoginModal />}{' '}
      <Button id="reset-btn" onClick={handleReset} color="error">
        App Reset
      </Button>{' '}
      <Button id="setup-btn" onClick={handleSetup} color="success">
        App Setup
      </Button>
    </div>
  );
};
