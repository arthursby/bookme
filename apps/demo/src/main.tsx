import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './app/app';
import { store } from './app/context/store';
import { SnackbarProvider } from 'notistack';

ReactDOM.render(
  <div>
    <SnackbarProvider maxSnack={3}>
      <Provider store={store}>
        <App />
      </Provider>
    </SnackbarProvider>
  </div>,
  document.getElementById('root')
);
