export interface IRoom {
  id: number;
  name: string;
  color: string;
}
