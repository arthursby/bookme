import { Observable } from 'rxjs';
import { IRoom } from './room.interface';

export interface IRoomList {
  rooms: IRoom[];
}

export interface IRoomsService {
  getAllRooms(params: unknown): Observable<IRoomList>;
}
