import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { ICreateUser } from '..';

export class CreateUserDto implements ICreateUser {
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(7)
  password: string;
}
