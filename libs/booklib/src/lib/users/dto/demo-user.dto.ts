import { ICreateUser } from '..';

export const demoUser: ICreateUser = {
  email: 'demo@consensys.com',
  password: 'Findag00dpassw0rd',
};

export const demoUser2: ICreateUser = {
  email: 'demo2@consensys.com',
  password: 'Findag00dpassw0rd',
};
