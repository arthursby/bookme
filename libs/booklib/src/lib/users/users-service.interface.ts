import { ICreateUser, IUser } from '.';

export interface IUsersService {
  createUser(createUserDto: ICreateUser): Promise<IUser>;
  findById(id: number): Promise<IUser>;
  findByEmail(email: string): Promise<IUser>;
  remove(id: number): void;
}
