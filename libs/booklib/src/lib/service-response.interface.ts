export interface IRPCResponse {
  status: number;
  message?: string;
  data?: unknown;
  error?: unknown;
}
