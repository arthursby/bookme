export interface ITracker {
  requestId: string;
  sessionId: string;
  email?: string;
  userId?: number;
}
