import { IReservation } from '..';

export interface IVerifyReservationResult {
  count: number;
  reservations?: IReservation[];
}
export class VerifyReservationResultDto implements IVerifyReservationResult {
  count: number;
  reservations?: IReservation[];
}
