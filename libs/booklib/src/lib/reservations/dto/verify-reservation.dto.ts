import { Type } from 'class-transformer';
import { IsDate, IsNotEmpty, IsNumber } from 'class-validator';

export class VerifyReservationDto {
  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  start: Date;
  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  end: Date;
  @Type(() => Number)
  @IsNumber()
  roomId: number;
}
