import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class DeleteReservationDto {
  @Type(() => Number)
  @IsNumber()
  @IsNotEmpty()
  id: number;
  @Type(() => Number)
  @IsNumber()
  @IsNotEmpty()
  userId: number;
}
