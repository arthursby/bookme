import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateReservationDto {
  start: Date;
  end: Date;
  @IsNumber()
  userId: number;
  @IsNumber()
  roomId: number;
  @IsNotEmpty()
  title: string;
  color?: string;
}
