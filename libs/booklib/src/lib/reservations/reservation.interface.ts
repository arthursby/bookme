export interface IReservation {
  id: number;
  title: string;
  start: Date;
  end: Date;
  // no foreign keys since we are in isolated microservices
  userId: number;
  roomId: number;
  color?: string;
}
