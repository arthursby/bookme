import { DeleteResult } from 'typeorm';
import {
  CreateReservationDto,
  DeleteReservationDto,
  FindReservationDto,
  IReservation,
  UpdateReservationDto,
  VerifyReservationDto,
  VerifyReservationResultDto,
} from '.';
import { IPaginatedResult, PaginationParams } from '../utils/pagination';

export interface IReservationsService {
  create(reservationDto: CreateReservationDto): Promise<IReservation>;
  findById(reservationId: number): Promise<IReservation>;
  find(
    findReservationDto: FindReservationDto,
    params?: PaginationParams
  ): Promise<IPaginatedResult<IReservation>>;
  update(reservationDto: UpdateReservationDto): Promise<IReservation>;
  remove(deleteReservationDto: DeleteReservationDto): Promise<DeleteResult>;
  verifyAvailability(
    reservationInfo: VerifyReservationDto
  ): Promise<VerifyReservationResultDto>;
}
