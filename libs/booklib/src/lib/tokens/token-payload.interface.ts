import { IUser } from '../users';

export interface ITokenPayload {
  sub: string;
  user_id: number;
  user?: IUser; // might be going away
  creation: number; // time of creation in ms
}
