export interface IToken {
  user_id: number;
  refresh_token: string;
  expires_at: Date;
}
