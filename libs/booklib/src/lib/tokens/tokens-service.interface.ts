import { ITokenPayload, ITokenResponse } from '.';
import { IUser } from '../users';

export interface ITokenService {
  createToken(user: IUser): Promise<ITokenResponse>;
  deleteTokenForUserId(userId: number): void;
  verifyToken(token: string): Promise<ITokenPayload>;
  decodeToken(token: string):
    | string
    | {
        [key: string]: unknown;
      };
}
