import { IUser } from '../users';

export interface ITokenResponse {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  expires_at?: number;
  refresh_token_expires_in: number;
  refresh_token_expires_at?: number;
  user: IUser;
}
