import { LogInDto, RegisterDto, SilentLoginDto } from '.';
import { ITokenResponse } from '../tokens';

export interface IAuthService {
  register(registrationData: RegisterDto): Promise<ITokenResponse>;
  login(credentials: LogInDto): Promise<ITokenResponse>;
  silentLogin(credentials: SilentLoginDto): Promise<ITokenResponse>;
  logout(userId: number): void;
}
