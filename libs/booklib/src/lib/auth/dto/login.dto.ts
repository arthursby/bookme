import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { ILogin } from '..';

export class LogInDto implements ILogin {
  @IsEmail()
  email!: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(7)
  password!: string;
}

export default LogInDto;
