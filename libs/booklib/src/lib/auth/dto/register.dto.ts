import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { IRegister } from '../register.interface';

export class RegisterDto implements IRegister {
  @IsEmail()
  email: string;

  // @IsString()
  // @IsNotEmpty()
  // name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(7)
  password: string;

  // @IsString()
  // @IsNotEmpty()
  // @Matches(/^\+[1-9]\d{1,14}$/)
  // phoneNumber: string;
}

export default RegisterDto;
