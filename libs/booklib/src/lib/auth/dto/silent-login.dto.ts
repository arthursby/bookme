import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class SilentLoginDto {
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(20)
  refreshToken: string;
}
