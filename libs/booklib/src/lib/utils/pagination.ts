import { Type } from 'class-transformer';
import { IsNumber, IsOptional, Min } from 'class-validator';

export class PaginationParams {
  @IsOptional()
  search?: string;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(1)
  startId?: number;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(0)
  offset?: number;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(1)
  limit?: number;

  @IsOptional()
  order: string[];

  @IsOptional()
  direction: ('ASC' | 'DESC')[];
}

export class PaginationParamsOrder {
  direction: 'ASC' | 'DESC';
  field: string;
}

export interface IPaginatedResult<T> {
  data: T[];
  total: number;
  current: number;
}
