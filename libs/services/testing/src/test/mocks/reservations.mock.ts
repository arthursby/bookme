import { CreateReservationDto } from "@bookme/booklib";
import {addDays, addMinutes, addHours} from 'date-fns';

const today = new Date();

export const createReservationSuccess:CreateReservationDto = {
  start: addMinutes(today, 1),
  end: addMinutes(today, 61),
  userId: 1,
  roomId: 1,
  title: "cannot be empty",
  color: ""
}

// const createReservation = ({userId, roomId?, start?, comment?}:{}):CreateReservationDto => {
//   return {}
// }