import { ICreateUser } from "@bookme/booklib";

export const userCreateSuccess:ICreateUser = {
  email: 'valid@email.com',
  password: 'changeme',
};

export const userCreateErrorInvalidEmail:ICreateUser = {
  email: 'invalid',
  password: 'changeme',
};

export const userCreateErrorPasswordLength:ICreateUser = {
  email: 'invalid',
  password: 'short',
};

