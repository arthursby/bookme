import { ITracker } from '@bookme/booklib';

export interface ITrackedPayload<T> {
  tracking: ITracker;
  data: T;
}
