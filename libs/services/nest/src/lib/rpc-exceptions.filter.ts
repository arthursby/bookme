import {
  ArgumentsHost,
  Catch,
  Logger,
  RpcExceptionFilter,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Observable, throwError } from 'rxjs';

@Catch(RpcException)
export class RPCExceptionsFilter implements RpcExceptionFilter<RpcException> {
  private readonly logger = new Logger(RPCExceptionsFilter.name);

  catch(exception: RpcException, host: ArgumentsHost): Observable<any> {
    const ctx = host.switchToRpc();
    const data = ctx.getData();

    const error = {
      message: exception.message,
      timestamp: new Date().toISOString(),
      data: data,
    };

    this.logger.debug(
      `Uncaught error at ${error.timestamp} ${JSON.stringify(
        exception
      )} data=${JSON.stringify(data)}`
    );

    return throwError(() => error);
  }
}
