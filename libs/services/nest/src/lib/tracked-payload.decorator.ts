import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const TrackedPayload = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    let payload;
    if (context.getType() === 'rpc') {
      const ctx = context.switchToRpc();

      payload = ctx.getData();
      if (payload.tracking) {
        return payload.data;
      }
    }
    return payload;
  }
);
