import { ITracker } from '@bookme/booklib';
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger(LoggingInterceptor.name);

  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    let path = context.getClass().name + '.' + context.getHandler().name;

    let trackingInfo: ITracker;
    if (context.getType() == 'http') {
      const ctx = context.switchToHttp();
      const request = ctx.getRequest<Request>();
      path = request.url;
      const requestId = request.headers['x-request-id'];
      const sessionId = request.headers['x-session-id'];
      const userId = request.headers['x-user-id'];
      const email = request.headers['email'];
      trackingInfo = {
        requestId,
        sessionId,
        userId,
        email,
      };
    } else if (context.getType() == 'rpc') {
      const ctx = context.switchToRpc();
      const data = ctx.getData();
      if (data.tracking) {
        trackingInfo = data.tracking;
      }
    }
    this.logger.debug(
      `${path} request=${trackingInfo?.requestId} session=${trackingInfo?.sessionId} user=${trackingInfo?.userId} [${trackingInfo?.email}]`
    );
    return next.handle();
  }
}
