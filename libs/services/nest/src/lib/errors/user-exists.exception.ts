import { APP_ERRORS } from '@bookme/booklib';
import { RpcException } from '@nestjs/microservices';

export class UserExistsException extends RpcException {
  constructor() {
    super(APP_ERRORS.USER_EXISTS);
  }
}
