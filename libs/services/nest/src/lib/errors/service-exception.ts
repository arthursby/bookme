import { APP_ERRORS } from '@bookme/booklib';
import { RpcException } from '@nestjs/microservices';

export class ServiceException extends RpcException {
  constructor(type: APP_ERRORS, message?: string) {
    super({ type, message });
  }
}
