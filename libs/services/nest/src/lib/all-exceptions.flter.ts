import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { RpcException } from '@nestjs/microservices';
import { HttpExceptionsFilter, RPCExceptionsFilter } from '..';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger = new Logger(RPCExceptionsFilter.name);

  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost): void {
    // In certain situations `httpAdapter` might not be available in the
    // constructor method, thus we should resolve it here.
    const hostType = host.getType();

    this.logger.debug(
      `Global Exception on host type=${hostType} exceptionType=${JSON.stringify(
        exception
      )}`
    );

    if (hostType == 'http') {
      const filter = new HttpExceptionsFilter();
      return filter.catch(exception as HttpException, host);
    } else if (hostType == 'rpc') {
      const filter = new RPCExceptionsFilter();
      filter.catch(exception as RpcException, host);
    }
  }
}
