import { ITracker, IUser } from '@bookme/booklib';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const Tracker = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const requestId = request['id'];
    const sessionId = request['session'].id;
    const user: IUser = request['user'];
    const trackingInfo: ITracker = {
      requestId,
      sessionId,
    };
    if (user) {
      trackingInfo.email = user.email;
      trackingInfo.userId = user.id;
    }
    return trackingInfo;
  }
);
