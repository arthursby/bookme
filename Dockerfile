FROM node:17-alpine3.14 as builder

ARG NODE_ENV
ARG BUILD_FLAG
WORKDIR /app/builder
COPY . .
RUN npm i

## Used to generate /node_modules
# Contains all the shared libraries for sub projects
# Needs to be rebuilt everytime a package is added / removed / upgraded